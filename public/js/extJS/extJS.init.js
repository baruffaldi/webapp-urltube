/*
* ExtJS 2.1 Configuration
*/
Ext.onReady(function(){
	Ext.util.Format.euMoney = function(v){
	v = (Math.round((v-0)*100))/100;
	v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
	return ('&euro; ' + v);
	};
	
	// Default headers to pass foreach request
	Ext.Ajax.defaultHeaders = {
	'Powered-By': 'BF CMS'
	};
	
	// Enable quicktips handler
	Ext.QuickTips.init();
	
	// Setting stateManager Cookie Provider
	Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
	
	// turn on validation errors beside the field globally    
	Ext.form.Field.prototype.msgTarget = 'side';
	
	// Login status Animated Window PopUp

	Ext.dspLoginBox = function(){
	    var msgCt;
	
	    function createBox(t, s){
	        return ['<div class="msg">',
	                '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
	                '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3>', t, '</h3>', s, '</div></div></div>',
	                '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
	                '</div>'].join('');
	    }
	    return {
	        msg : function(title, format){
	            if(!msgCt){
	                msgCt = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
	            }
	            msgCt.alignTo(document, 't-t');
	            var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
	            var m = Ext.DomHelper.append(msgCt, {html:createBox(title, s)}, true);
	            m.slideIn('t').pause(1).ghost("t", {remove:true});
	        },
	
	        init : function(){
	            var t = Ext.get('exttheme');
	            if(!t){ // run locally?
	                return;
	            }
	            var theme = Cookies.get('exttheme') || 'aero';
	            if(theme){
	                t.dom.value = theme;
	                Ext.getBody().addClass('x-'+theme);
	            }
	            t.on('change', function(){
	                Cookies.set('exttheme', t.getValue());
	                setTimeout(function(){
	                    window.location.reload();
	                }, 250);
	            });
	
	            var lb = Ext.get('lib-bar');
	            if(lb){
	                lb.show();
	            }
	        }
	    };
	}();

  // Blank image
  Ext.BLANK_IMAGE_URL = '/web/images/default/s.gif';

  // Events Listener
});
/*
* Other stuffs
*/
// Dumper function


function notifyFatalError( transport, opt )
{
	   loadingWindowHandler.show();
    	Ext.Ajax.request({
        url:     'notifyFatalError?method='+opt.method+'&page='+opt.url+'&status='+transport.status+'&statusText='+transport.statusText,
        method:  'get',
        callback: loadingWindowHandler.hide(),
        success: function() {Ext.MessageBox.alert( 'Fatal Error Notifier', 'The Fatal Error has been notified! Please retry later');},
        failed: function() {Ext.MessageBox.alert( 'Fatal Error Notifier', 'Unable to notify the Fatal Error to the developers. Please do it yourself ;)');}
     });
}
