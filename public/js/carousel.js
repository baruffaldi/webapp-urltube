var loadInitialItems = function(type, args) {

    var start = args[0];
    var last = args[1]; 
    // fetch twice the number for caching. images are create once.

    // Illustrates setting the size during the load. Set silent parameter to true   
    makeRequest(this, '/ajax/get-carousel-data', 'most-visited', start, last);    
};

var loadInitialItemsRecents = function(type, args) {

    var start = args[0];
    var last = args[1]; 
    
    // fetch twice the number for caching. images are create once.
    makeRequest(this, '/ajax/get-carousel-data', "recent-contents", start, (last-start+1) * 2); 
}
/**
 * Custom load next handler. Called when the carousel loads the next
 * set of data items. Specified to the carousel as the configuration
 * parameter: loadNextHandler
 */
var loadNextItems = function(type, args) {    

    var start = args[0];
    var last = args[1];
    var alreadyCached = args[2];
    
    if(!alreadyCached) {
        makeRequest(this, '/ajax/get-carousel-data', "recent-contents", start, (last-start+1) * 2);
    }

};

/**
 * Custom load previous handler. Called when the carousel loads the previous
 * set of data items. Specified to the carousel as the configuration
 * parameter: loadPrevHandler
 */
var loadPrevItems = function(type, args) {
    var start = args[0];
    var last = args[1]; 
    var alreadyCached = args[2];

    if(!alreadyCached) {
        makeRequest(this, '/ajax/get-carousel-data', "recent-contents", start, (last-start+1) * 2);
    }
};


/**
 * You must create the carousel after the page is loaded since it is
 * dependent on an HTML element (in this case 'dhtml-carousel'.) See the
 * HTML code below.
 */
var most_visited;
var recent_urls; // for ease of debugging; globals generally not a good idea

var pageLoad = function() 
{
    recent_urls = new YAHOO.extension.Carousel("dhtml-carousel", 
        {
            numVisible:        8,
            navMargin:         40,
            loadInitHandler:   loadInitialItems
        }
    );
    most_visited = new YAHOO.extension.Carousel("dhtml-carousel2", 
        {
            numVisible:        8,
            animationSpeed:    1.30,
            navMargin:         40,
			autoPlay:          10000,
			wrap:              true,
            loadInitHandler:   loadInitialItemsRecents,
            prevElement:       "left-arrow",
            nextElement:       "right-arrow",
            loadNextHandler:   loadNextItems,
            loadPrevHandler:   loadPrevItems,
            prevButtonStateHandler:   handlePrevButtonState,
            nextButtonStateHandler:   handleNextButtonState
        }
    );
    //most_visited.autoPlay();
};

/**
 * Custom button state handler for enabling/disabling button state. 
 * Called when the carousel has determined that the previous button
 * state should be changed.
 * Specified to the carousel as the configuration
 * parameter: prevButtonStateHandler
 */
var handlePrevButtonState = function(type, args) {

    var enabling = args[0];
    var leftImage = args[1];
    if(enabling) {
        leftImage.src = "/public/js/carousel/images/left-enabled.gif";    
    } else {
        leftImage.src = "/public/js/carousel/images/left-disabled.gif";    
    }
};

var handleNextButtonState = function(type, args) {
    var enabling = args[0];
    var rightImage = args[1];
    
    if(enabling) {
        rightImage.src = "/public/js/carousel/images/right-enabled.gif";
    } else {
        rightImage.src = "/public/js/carousel/images/right-disabled.gif";
    }
    
};

var showButtons = function(type, args) {
    YAHOO.util.Dom.setStyle("left-arrow", "visibility", "visible");
    YAHOO.util.Dom.setStyle("left-arrow", "visibility", "visible");
};

YAHOO.util.Event.addListener(window, 'load', pageLoad);

/**
 * Called via the YUI Connection manager (see makeRequest).
 */
var handleSuccess = function(callbackResponse)
{
    var start = callbackResponse.argument[0];
    var numResults = callbackResponse.argument[1];
    var carousel = callbackResponse.argument[2];
    
      if(callbackResponse.responseText !== undefined) {
        var theTrip = eval( '(' + callbackResponse.responseText + ')' );
        for(var i=0; i< theTrip.ResultSet.totalResultsReturned; i++) {
            var result = theTrip.ResultSet.Result[i];
            carousel.addItem(start+i, fmtTripInnerHTML(result));
        }
        showButtons();
     }
};

/**
 * Since carousel.addItem uses an HTML string to create the interface
 * for each carousel item, this method takes an individual trip plan
 * result and cobbles together HTML for the innerHTML argument.
 */
var fmtTripInnerHTML = function(result) {

      var tripInnerHTML = 
          '<a href="' + 
          result.Url + 
          '"><img src="' + 
          result.Image.Url +
        '" width="' +
        result.Image.Width +
        '" height="' +
        result.Image.Height+
        '"/>' + 
          trunc(result.Title, 40, 20) + 
          '<\/a>' + 
          trunc(result.Summary, 20, 20);
  
    return tripInnerHTML;
    
};

var handleFailure = function(o)
{
     var result = o.status + " " + o.statusText;
};
  
/**
 * A utility function for invoking the YUI connection manager (Ajax)
 * with a URL that matches the Yahoo! developer network Trip Planner
 * APIs (see: http://developer.yahoo.com/travel/tripservice/V1/tripSearch.html)
 *
 * The callback object is the configuration object for the YUI Connection
 * manager. If this is successful, the 'handleSuccess' function is called.
 */
var makeRequest = function(carousel, url, query, start, numResults)
{
    var params = '?query=' + query + 
                            '&start=' + start + 
                            '&limit=' + carousel.getProperty( 'numVisible' ); 
    
    var callback =
    {
          success: handleSuccess,
          failure: handleFailure,
          argument: [start, numResults, carousel]
    };
    
    var sUrl = url + params; 
    YAHOO.util.Connect.asyncRequest("GET", sUrl, callback, null);
};

/**
 * Just a utility function for cleaning up the returned HTML response
 * and truncating it.
 */
var trunc = function(str, maxLen, maxWordLen) 
{
    // Strip markup
    str = str.replace("<b>", "");
    str = str.replace("<\/b>", "");
    str = str.replace("<B>", "");
    str = str.replace("<\/B>", "");
    
    // Simple truncation
    if(str.length > maxLen) {
        str = str.substring(0,maxLen) + "...";
    }

    // Truncate for long words
    var start = 0;
    var loopCnt = 0;
    var strSlice = str;
    
    do  {
        var spaceBreak = strSlice.indexOf(' ');
        var lenOfWord = spaceBreak;
        if(lenOfWord == -1)
        {
            lenOfWord = strSlice.length;
        }

        if (lenOfWord > maxWordLen) {
            //debugMsg("Long word found in: " + strSlice);
            str = str.substring(0, maxWordLen);  // TRUNCATE
        }
        start = spaceBreak+1;
        strSlice = strSlice.substring(start);
        spaceBreak = strSlice.indexOf(' ');
    } while(spaceBreak != -1)
    
    
    return str;
};