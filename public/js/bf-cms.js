var BFCMS = {
	start:function(){
	BFCMS.resetForm( 'addLinkForm' );
	},
	
	switchImage:function( element, image ){ 
		element.src = "/public/images/" + image; 
	},
	
	switchBgImage:function( element, image ){ 
		element.style.backgroundImage = "url( \"/public"+ image+"\" )"; 
	},
	
	switchButtonImage:function( element, image, hover )
	{ 
	if ( hover ) element.style.backgroundImage = "url( \'/public/images/index." + image + ".icon.hover.jpg\' )"; 
	else element.style.backgroundImage = "url( \'/public/images/index." + image + ".icon.jpg\' )"; 
	},
	
	dump:function( what, level )
	{
		var dumped_text = "";
		if(!level) level = 0;
	
		//The padding given at the beginning of the line.
		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";
	
		if(typeof(arr) == 'object') { //Array/Hashes/Objects
			for(var item in what) {
				var value = what[item];
	
				if(typeof(value) == 'object') { //If it is an array,
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value,level+1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else { //Stings/Chars/Numbers etc.
			dumped_text = "===>"+what+"<===("+typeof(what)+")";
		}
		return dumped_text;
	},
	
	getVar:function( what ) {
         get_string = document.location.search;         
         return_value = '';
         
         do { //This loop is made to catch all instances of any get variable.
            name_index = get_string.indexOf(name + '=');
            
            if(name_index != -1)
              {
              get_string = get_string.substr(name_index + name.length + 1, get_string.length - name_index);
              
              end_of_value = get_string.indexOf('&');
              if(end_of_value != -1)                
                value = get_string.substr(0, end_of_value);                
              else                
                value = get_string;                
                
              if(return_value == '' || value == '')
                 return_value += value;
              else
                 return_value += ', ' + value;
              }
            } while(name_index != -1)
            
         //Restores all the blank spaces.
         space = return_value.indexOf('+');
         while(space != -1)
              { 
              return_value = return_value.substr(0, space) + ' ' + 
              return_value.substr(space + 1, return_value.length);
							 
              space = return_value.indexOf('+');
              }
          
         return(return_value);        
	},
	
	resetForm:function( formId, action )
	{
		if ( action != '' ) document.getElementById(formId).action = action;
		else document.getElementById(formId).action = '#';
	}
}

var UrlTube = {
	start:function(){
	},
	
	checkDomainAvailability:function(input){
		var box;
		box = document.getElementById( 'checkDomainAvailability' );
		if ( box != null )
			box.innerHTML = '<img src="/public/images/help.png" style="width: 10px;" alt="OK!" />';

	},
	
	submitNewUrl:function( form )
	{
		var div;
		document.getElementById( 'addLinkForm' ).action = '/ajax/create-short-internet-link';
		
		var myHTMLRequest = new Request({url:'/ajax/create-short-internet-link',
		                                      onRequest:function(){document.getElementById( 'addLink' ).style.textAlign = 'center';document.getElementById( 'addLink' ).style.border = '0px';document.getElementById( 'addLink' ).innerHTML = '<img style="margin: 0 auto; border: 0px solid transparent;" src="/public/images/loadingAnimation.gif" alt="zZzZz" />';},
		                                      onSuccess:function(r,rX){document.getElementById( 'addLink' ).style.textAlign = 'left';document.getElementById( 'addLink' ).style.border = '3px';document.getElementById( 'addLink' ).innerHTML = r;}
		                                     });

		var postVars = 'add_url=' + document.getElementById( 'add_url' ).value +
		               '&add_domain=' + document.getElementById( 'add_domain' ).value +
		               '&add_public=' + document.getElementById( 'add_public' ).value +
		               '&add_title=' + document.getElementById( 'add_title' ).value +
		               '&add_description=' + document.getElementById( 'add_description' ).value;/*
		               '&add_masquerading=' + document.getElementById( 'add_masquerading' ).value +
		               '&add_notify_email=' + document.getElementById( 'add_notify_email' ).value +
		               '&add_irc_notify_hostname=' + document.getElementById( 'add_irc_notify_hostname' ).value +
		               '&add_irc_notify_port=' + document.getElementById( 'add_irc_notify_port' ).value +
		               '&add_irc_notify_channel=' + document.getElementById( 'add_irc_notify_channel' ).value;*/
		
		myHTMLRequest.send( postVars );
		
	},
	
	submitForm:function( formId, divId, url )
	{
		document.getElementById( formId ).action = url;
		$(formId).send({
				onRequest: function(){
					// Show loading div.
					document.getElementById( divId ).style.textAlign = 'center';
					document.getElementById( divId ).style.border = '0px';
					document.getElementById( divId ).innerHTML = '<img style="margin: 0 auto; border: 0px solid transparent;" src="/public/images/loadingAnimation.gif" alt="zZzZz" />';
				},
				onSuccess: function(){
					document.getElementById( divId ).style.textAlign = 'left';
					document.getElementById( divId ).innerHTML = r;
				},
				onFailure: function(){
					document.getElementById( divId ).style.textAlign = 'left';
					document.getElementById( divId ).innerHTML = 'Failed! <a href="/index/share-your-url?add_url='+1+'&add_url='+1+'&add_url='+1+'&add_url='+1+'&add_url='+1+'&add_url='+1+'" title="Retry!">Click here to retry!</a>';
				}
			});
	},
	
	processAHref:function( )
	{/* Ciclo che prende tutte le anchors e se non hanno come 
		classe thickbox, cambia l'url in '#' ed aggiunge l'evento 
		onclick='javascript:getPage(url)' con controllo in modo che 
		NON sia remoto */
	},

   makeRequest:function(url, parameters, divId ) {
      new Ajax(url + parameters, {
		method: 'get',
	}).request(parameters,{
		onRequest: function(){
			// Show loading div.
			var div = document.getElementById( divId );
			div.style.textAlign = 'center';
			div.style.border = '0px';
			div.innerHTML = '<img style="margin: 0 auto; border: 0px solid transparent;" src="/public/images/loadingAnimation.gif" alt="zZzZz" />';
		},
		onSuccess: function(){
			var div = document.getElementById( divId );
			div.style.textAlign = 'left';
			div.innerHTML = r;
		},
		onFailure: function(){
			var div = document.getElementById( divId );
			div.style.textAlign = 'left';
			div.innerHTML = 'Failed! <a href="/index/share-your-url?add_url='+1+'&add_url='+1+'&add_url='+1+'&add_url='+1+'&add_url='+1+'&add_url='+1+'" title="Retry!">Click here to retry!</a>';
		}
	});
   },
   
   get: function(obj,divId, url) {
      var getstr = "?";
      for (i=0; i<obj.childNodes.length; i++) {
         if (obj.childNodes[i].tagName == "input") {
            if (obj.childNodes[i].type == "text") {
               getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
            }
            if (obj.childNodes[i].type == "checkbox") {
               if (obj.childNodes[i].checked) {
                  getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
               } else {
                  getstr += obj.childNodes[i].name + "=&";
               }
            }
            if (obj.childNodes[i].type == "radio") {
               if (obj.childNodes[i].checked) {
                  getstr += obj.childNodes[i].name + "=" + obj.childNodes[i].value + "&";
               }
            }
         }   
         if (obj.childNodes[i].tagName == "select") {
            var sel = obj.childNodes[i];
            getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
         }
         
      }
      UrlTube.makeRequest(url, getstr, divId);
   },
   
	 ajaxHijackForm:function( formId, divId, ajaxactionurl )
	 {
	 	var form = document.getElementById(formId);
		 var action_url = form.action;
		 form.action = 'javascript:UrlTube.get(document.getElementById(\"'+formId+'\"),\"'+divId+'\",\"'+ajaxactionurl+'\")';
	 }

}

window.onload = function() {
	BFCMS.start;
	UrlTube.start;
}

//UrlTube.checkDomainAvailability(this);

var path= "/public/images/";
var blank=path+"blank.gif"

function fnLoadPngs() {
  var rslt = navigator.appVersion.match(/MSIE (\d+\.\d+)/, '');
  var itsAllGood = (rslt != null && Number(rslt[1]) >= 5.5);

  for (var i = document.images.length - 1, img = null; (img = document.images[i]); i--) {
    if (itsAllGood && img.src.match(/\.png$/i) != null) {
      img.style.visibility = "hidden";
      fnFixPng(img);
      img.attachEvent("onpropertychange", fnPropertyChanged);
    }
    img.style.visibility = "visible";
  }

  var nl = document.getElementsByTagName("INPUT");
  for (var i = nl.length - 1, e = null; (e = nl[i]); i--) {
    if (e.className && e.className.match(/\bimage\b/i) != null) {
      if (e.src.match(/\.png$/i) != null) {
        e.style.visibility = "hidden";
        fnFixPng(e);
        e.attachEvent("onpropertychange", fnPropertyChanged);
      }
    }
    e.style.visibility = "visible";
  }
}

function fnPropertyChanged() {
  if (window.event.propertyName == "src") {
    var el = window.event.srcElement;
    if (!el.src.match(/spacer\.gif$/i)) {
      el.filters.item(0).src = el.src;
      el.src = blank;
    }
  }
}

function dbg(o) {
  var s = "";
  var i = 0;
  for (var p in o) {
    s += p + ": " + o[p] + "\n";
    if (++i % 10 == 0) {
      alert(s);
      s = "";
    }
  }
  alert(s);
}

function fnFixPng(img) {

  var src = img.src;
  img.style.width = img.width + "px";
  img.style.height = img.height + "px";
  img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "', sizingMethod='scale')"
  img.src = blank;
}


  if (navigator.platform == "Win32" && navigator.appName == "Microsoft Internet Explorer" && window.attachEvent) {
    document.writeln('<style type="text/css">img, input.image { visibility:hidden; } </style>');
    window.attachEvent("onload", fnLoadPngs);
}