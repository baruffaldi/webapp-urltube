function googleSearch() { 	
	// web search, open, alternate root 
	var localoptions = new google.search.SearcherOptions(); 
	localoptions.setExpandMode( google.search.SearchControl.EXPAND_MODE_OPEN ); 
	localoptions.setRoot( document.getElementById( "search_content" ) ); 

	siteSearch = new google.search.WebSearch();
	siteSearch.setUserDefinedLabel( "Urls" );
	siteSearch.setSiteRestriction( "urltube.net" );

	// create a search control 
	var searchControl = new google.search.SearchControl(); 
	searchControl.addSearcher( siteSearch, localoptions );
	searchControl.draw( document.getElementById( "search_control" ) ); 
} 

google.load( 'search', '1.0'); 
google.setOnLoadCallback( googleSearch, true ); 