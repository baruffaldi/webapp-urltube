<?php
/**
 * MV-Controller: Browse Front-End
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */
class BrowseController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'index', 'index' ); }
    
    public function categoryAction( ) { }
    
    public function urlAction( ) 
    { 
    	global $_SITE;
    	
	    $url = array_keys( $_GET );
	    $url = $url[0];
		
	    $_SITE['urlTube']['url'] = UrlTube::getSilBySilDomain( $url );
	}
}