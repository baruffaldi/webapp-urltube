<?php
/**
 * MV-Controller: Feeds
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

class FeedsController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'feeds', 'index' ); }
    
    public function allXmlAction( ) { }
    
    public function webXmlAction( ) { }
    
    public function ftpXmlAction( ) { }
    
    public function rtspXmlAction( ) { }
    
    public function textXmlAction( ) { }
    
    public function flashXmlAction( ) { }
    
    public function quicktimeXmlAction( ) { }
    
    public function audioXmlAction( ) { }
    
    public function imageXmlAction( ) { }
    
    public function videoXmlAction( ) { }
    
    public function pdfXmlAction( ) { }
    
    public function mapsXmlAction( ) { }
    
    public function flickrXmlAction( ) { }
    
    public function youtubeXmlAction( ) { }
    
    public function googleXmlAction( ) { }
    
    public function sitemapXmlAction( ) { }
    
    public function sitemapXslAction( ) { }
}