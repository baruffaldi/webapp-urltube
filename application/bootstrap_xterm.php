<?php
/**
 * Application bootstrap
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage ShortInternetLink
 */

// *** Production Enabler
     $_SITE['config']['env']['type']  = $_SERVER['argv'][1];
// *** Default Settings
     $_SITE['config']['env']['language'] = array( 'full' => 'en-US', 'short' => 'en' );
     $_SITE['config']['env']['theme'] = 'default';

// *** CMS Settings 
     define( '__VERSION__', '1.3' );
     define( '__GENERATOR__', "BF CMS " . __VERSION__ );

// *** Environment Settings 
     $_SITE['config']['fs']['path_documentroot']  = dirname( __FILE__ )                         . DIRECTORY_SEPARATOR . '..';
     $_SITE['config']['fs']['path_public']        = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 'public';
     $_SITE['config']['fs']['path_lib']           = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 'library';
     $_SITE['config']['fs']['path_app']           = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 'application';
     $_SITE['config']['fs']['path_bootstrap']     = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'bootstrap';
     $_SITE['config']['fs']['path_templates']     = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'templates';
     $_SITE['config']['fs']['path_config']        = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'config';
     $_SITE['config']['fs']['path_log']           = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'logs';

// *** PHP Settings 
     $_SITE['config']['env']['debug'] = ( isset( $_GET['debug'] ) 
                                       || isset( $_POST['debug'] ) 
                                       || $_SERVER['ENV'] == 'development' ) ? TRUE : FALSE;
                                       
     set_include_path( get_include_path( ) . PATH_SEPARATOR 
                     . $_SITE['config']['fs']['path_lib'] );
     
     if ( $_SITE['config']['env']['debug'] ) error_reporting( E_ALL ^ E_NOTICE );
     if ( $_SITE['config']['env']['debug'] ) ini_set( 'display_errors', 1 );
     else ini_set( 'display_errors', 0 );

// *** Configuration Handler
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'config.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'config.php';

// *** Include all Classes
     foreach ( explode( ',', $_SITE['config']['handler']->init->classes_xterm ) as $class )
         if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Classes' 
                                                              . DIRECTORY_SEPARATOR . trim( $class ) . '.php' ) ) 
              require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Classes' 
                                                              . DIRECTORY_SEPARATOR . trim( $class ) . '.php';
     
     unset( $class );

// *** Include urlTube Classes

     foreach ( explode( ',', $_SITE['config']['handler']->init->urlTube ) as $urlTube )
         if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'UrlTube' 
                                                              . DIRECTORY_SEPARATOR . trim( $urlTube ) . '.php' ) ) 
              require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'UrlTube' 
                                                              . DIRECTORY_SEPARATOR . trim( $urlTube ) . '.php';
     
     unset( $urlTube );

// *** Include all init Modules
     foreach ( explode( ',', $_SITE['config']['handler']->init->modules_xterm ) as $initModule )
         if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . trim( $initModule ) . '.php' ) ) 
              require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . trim( $initModule ) . '.php';
     
     unset( $initModule );