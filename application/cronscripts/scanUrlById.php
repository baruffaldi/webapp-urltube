<?php
try { require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'bootstrap_xterm.php'; }
 
catch ( Exception $exception ) 
{

/**
 * @todo write a smarty template to get this courtesy message
 */
    echo '<html><body><center>'
       . 'An exception occured while bootstrapping the application.';
       
    if ($_SITE['config']['env']['type'] != 'production' ) 
        echo '<br /><br />' . $exception->getMessage( ) . '<br />'
           . '<div align="left">Stack Trace:' 
           . '<pre>' . $exception->getTraceAsString( ) . '</pre></div>';
    
    echo '</center></body></html>';
    exit( 1 );
}

define( '__DEBUG__', ( $_SERVER['argv'][2] == '1' ) ? TRUE : FALSE );

// NomeFile ripulito, risoluzione, formato, dimensione, sito di provenienza, percentuale nudita'?

$linksColumns = array( 'id', 'visits_counter', 'max_visits', 'name' );
$urlColumns = array( 'id_url', 'url', 'mimetype' );
$scansColumns = NULL;
$order = array( 'links.creation_date ASC' );
$active = 'Y';
$where = array( 'url.id_url = ?' => $_SERVER['argv'][3] );
$mimetype = NULL;
$service = NULL;
$protocol = NULL;

$url = UrlTube::getUrl( $linksColumns, $urlColumns, $scansColumns, $order, $active, $where, $mimetype, $service, $protocol );

if ( ! empty( $url['id'] ) )
{
     $_SITE['UrlTube']['Parse'] = new UrlTube_Parse();
     
     $mimetype = $_SITE['UrlTube']['Parse']->getDataByMimetype( $url['mimetype'] );
     $service = $_SITE['UrlTube']['Parse']->getServiceByURI( $url['url'] );
     
     if ( __DEBUG__ )
     {
          var_dump( $url['url'] );
          var_dump( $url['mimetype'] );
          var_dump( $service );
     }
               
     switch( $service ) 
     {          
          case 'google':
               if ( __DEBUG__ )
                     var_dump( 'SCAN GOOGLE' );
               break;
               
          case 'facebook':
               if ( __DEBUG__ )
                     var_dump( 'SCAN FACEBOOK' );
               break;
               
          case 'youtube':
               if ( __DEBUG__ )
                     var_dump( 'SCAN YOUTUBE' );
               break;
               
          case 'picasa':
               if ( __DEBUG__ )
                     var_dump( 'SCAN PICASA' );
               break;
               
          case 'flickr':
               if ( __DEBUG__ )
                     var_dump( 'SCAN FLICKR' );
               break;
               
          case 'maps':
               if ( __DEBUG__ )
                     var_dump( 'SCAN MAPS' );
               break;
     }
     
     switch( $mimetype[2] )
     {
          case 'x-flv':
          case 'x-shockwave-flash':
          case 'futuresplash':
          case 'flash':
               if ( __DEBUG__ )
                     var_dump( 'SCAN FLASH' );
               break;
               
          case 'xml':
          case 'xhtml':
          case 'html':
               if ( __DEBUG__ )
                     var_dump( 'SCAN WEBSITE' );
               $scan = new Zend_UrlTube_Client( $url['url'] );
               $result = $scan->scanStatus( );
               $scan->scanStatusResult( $url, $result );
               $scan->scan( 'web', $url );
               break;
               
          case 'pdf':
               if ( __DEBUG__ )
                     var_dump( 'SCAN PDF' );
               break;
               
          case 'quicktime':
               if ( __DEBUG__ )
                     var_dump( 'SCAN QUICKTIME' );
               break;
     }
     
     switch( $mimetype[1] )
     {
          default:
               if ( __DEBUG__ )
                     var_dump( 'SCAN EVERYTHING ELSE' . $mimetype[1]  );
                     
               $scan = new Zend_UrlTube_Client( $url['url'] );
               $result = $scan->scanStatus();
               $scan->scanStatusResult( $url['url'], $result );
               break;
               
          case 'text':
               if ( __DEBUG__ )
                     var_dump( 'SCAN TEXT FILE' );
               break;
               
          case 'audio':
               if ( __DEBUG__ )
                     var_dump( 'SCAN AUDIO FILE' );
               break;
               
          case 'video':
               if ( __DEBUG__ )
                     var_dump( 'SCAN VIDEO FILE' );
               break;
               
          case 'image':
               if ( __DEBUG__ )
                     var_dump( 'SCAN IMAGE FILE' );
               $scan = new Zend_UrlTube_Client( $url['url'] );
               $result = $scan->scanStatus( );
               $scan->scanStatusResult( $url, $result );
               $scan->scan( 'image', $url );
               break;
     }
     // Devo trovare: nomedelfile, risoluzione, percentuale nudita', anteprima
} else {
     print "Nothing to do\n";
}
