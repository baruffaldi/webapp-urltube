<?php
try { require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'bootstrap_xterm.php'; }
 
catch ( Exception $exception ) 
{

/**
 * @todo write a smarty template to get this courtesy message
 */
    echo '<html><body><center>'
       . 'An exception occured while bootstrapping the application.';
       
    if ($_SITE['config']['env']['type'] != 'production' ) 
        echo '<br /><br />' . $exception->getMessage( ) . '<br />'
           . '<div align="left">Stack Trace:' 
           . '<pre>' . $exception->getTraceAsString( ) . '</pre></div>';
    
    echo '</center></body></html>';
    exit( 1 );
}

define( '__DEBUG__', ( $_SERVER['argv'][2] == '1' ) ? TRUE : FALSE );

$urlColumns = array( 'id_url', 'url', 'mimetype' );
$scansColumns = array( 'scan_date' );
$order = NULL;
$active = 'Y';
$where = array( );
$mimetype = UrlTube_Parse::getMimetypeByCategory( 'web' );
$service = NULL;
$protocol = NULL;
$paging = NULL;

//$urls = UrlTube::getUrls( array( 'name', 'visits_counter' ), array( 'id_url', 'url', 'mimetype' ), array( 'image' ), array( 'links.creation_date DESC', 'links.visits_counter DESC' ), 'Y', $mimetype, $category, $category, array( 'limit' => 10 ), TRUE, $where );
$urls = UrlTube::getUrlsToUpdate( $urlColumns, $scansColumns, $order, $active, $mimetype, $service, $protocol, $paging, FALSE, $where );

if ( is_array( $urls[0] ) )
{
     foreach( $urls as $url )
          $queue[$url['id_url']] = $url['scan_date'];
          
     if ( is_array( $queue ) )
          foreach( $queue as $id => $last_scan_date )
               if ( ! empty( $id ) && ( ! is_null( $last_scan_date ) || $last_scan_date <= ( time( ) - ( 86400 * 7 ) ) ) )
                    print UrlTube::foregroundProcess( 'php ' . $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 'cronscripts' . DIRECTORY_SEPARATOR . "scanUrlById.php {$_SERVER['argv'][1]} {$_SERVER['argv'][2]} $id" );
}