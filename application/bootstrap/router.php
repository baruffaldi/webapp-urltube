<?php
/**
 * Bootstrap: Site Router
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

$host = $_SERVER['HTTP_HOST'];

preg_match( '^((.*)\..*|.*)\..*^', $host, $host );
$_SITE['config']['env']['site']   = trim( $host[2] );

if ( $_SITE['config']['env']['type'] == 'development' )
     $_SITE['config']['env']['site'] = trim( str_replace( 'test', NULL, $_SITE['config']['env']['site'] ) );

if ( substr( $_SITE['config']['env']['site'], strlen( $_SITE['config']['env']['site'] ) - 1 ) == '.' )
     $_SITE['config']['env']['site'] = substr( $_SITE['config']['env']['site'], 0, - 1 );
     
$_SITE['config']['env']['domain'] = trim( $_SITE['config']['env']['site'] );

// *** Test environment cleaned

if ( empty( $_SITE['config']['env']['site'] ) || 
     is_null( $_SITE['config']['env']['site'] ) ||
     $_SITE['config']['env']['site'] == 'www' )
     $_SITE['config']['env']['site'] = 'default';
     
if ( ! file_exists( $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR 
     . 'layouts' . DIRECTORY_SEPARATOR . $_SITE['config']['env']['site'] ) ) 
         $_SITE['config']['env']['site'] = 'redirect';
         
if ( ! file_exists( $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR 
     . 'layouts' . DIRECTORY_SEPARATOR . $_SITE['config']['env']['site'] ) ) 
         $_SITE['config']['env']['site'] = 'default';

$_SITE['config']['fs']['path_controller'] = $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                            'controllers' . DIRECTORY_SEPARATOR . $_SITE['config']['env']['site'];
                                            
$_SITE['config']['fs']['path_layout']     = $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                            'layouts' . DIRECTORY_SEPARATOR . $_SITE['config']['env']['site'];
                                            
$_SITE['config']['fs']['path_view']       = $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                            'views' . DIRECTORY_SEPARATOR . $_SITE['config']['env']['site'];
