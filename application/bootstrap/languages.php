<?php
/**
 * Bootstrap: Multi-Language
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

// *** Let's parse the correct languages file
if ( isset( $_SESSION['language'] ) ) {
     $_SITE['language'] = parse_ini_file( $_SITE['config']['fs']['path_app'] . "/languages/{$_SESSION['language']}.ini" );
     $_SITE['config']['env']['language'] = array( 'full' => "{$_SESSION['language']}-" . strtoupper( $_SESSION['language'] ), 'short' => "{$_SESSION['language']}" );
// *** default themes
} elseif ( ! empty( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) ) {
     foreach( array_reverse( explode( ',', strtolower( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) ) ) as $lang )     
         if ( file_exists( $_SITE['config']['fs']['path_app'] . "/languages/" . array_pop( array_reverse( explode( '-', $lang ) ) ) . ".ini" ) )
         {
              $_SITE['config']['env']['language'] = array( 'full' => $lang, 'short' => array_pop( array_reverse( explode( '-', $lang ) ) ) );
             $_SITE['language'] = parse_ini_file( $_SITE['config']['fs']['path_app'] . "/languages/{$_SITE['config']['env']['language']['short']}.ini" );
         }
} elseif ( ! empty( $_SITE['config']['handler']->site->language ) ) {
     $_SITE['language'] = parse_ini_file( $_SITE['config']['fs']['path_app'] . "/languages/{$_SITE['config']['handler']->site->languageShort}.ini" );
     $_SITE['config']['env']['language'] = array( 'full' => $_SITE['config']['handler']->site->languageLong, 'short' => $_SITE['config']['handler']->site->languageShort );
} else {
     $_SITE['language'] = parse_ini_file( $_SITE['config']['fs']['path_app'] . "/languages/en.ini" );
     $_SITE['config']['env']['language'] = array( 'full' => 'en-US', 'short' => 'en' );
}

// *** Here we declare all language constants
foreach( $_SITE['language'] as $key => $value )
    define( '__LANG_' . strtoupper( str_replace( '.', '_', $key ) ) . '__', $value );

$_SITE['language']['legal.notices'] = file_get_contents( $_SITE['config']['fs']['path_app'] . "/languages/{$_SITE['config']['env']['language']['short']}.legal.txt" );
$_SITE['language']['legal.privacy'] = file_get_contents( $_SITE['config']['fs']['path_app'] . "/languages/{$_SITE['config']['env']['language']['short']}.privacy.txt" );