<?php
/**
 * Bootstrap: Authentication
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

require_once 'Zend' . DIRECTORY_SEPARATOR . 'Auth.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Auth' . DIRECTORY_SEPARATOR . 'Result.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Auth' . DIRECTORY_SEPARATOR . 'Adapter' . DIRECTORY_SEPARATOR . 'DbTable.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Auth' . DIRECTORY_SEPARATOR . 'Storage' . DIRECTORY_SEPARATOR . 'Session.php';

// *** Save a reference to the Singleton instance of Zend_Auth
  $_SITE['user']['handler'] = Zend_Auth::getInstance();

// *** Use 'DSPAdminInterface v*' instead of 'Zend_Auth'
  $_SITE['user']['handler']->setStorage( 
      new Zend_Auth_Storage_Session( "{$_SITE['config']['handler']->site->name} {$_SITE['config']['handler']->site->version}" ) );

// *** Define the authentication result constant
  $_SITE['user']['info'] = $_SITE['user']['handler']->getStorage( )->read( );
  
define ( '__IS_GUEST__', ( $_SITE['user']['info'] == FALSE || is_null( $_SITE['user']['info'] ) ) );
define ( '__IS_REG__',   ! is_null( $_SITE['user']['info'] ) && $_SITE['user']['info']->rank >= $_SITE['config']['handler']->users->rank->normal );
define ( '__IS_ADMIN__', ! is_null( $_SITE['user']['info'] ) && $_SITE['user']['info']->rank >= $_SITE['config']['handler']->users->rank->admins );