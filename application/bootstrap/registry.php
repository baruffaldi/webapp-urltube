<?php
/**
 * Bootstrap: Registry
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

require_once 'Zend' . DIRECTORY_SEPARATOR . 'Registry.php';

$_SITE['registry']['handler'] = Zend_Registry::getInstance();

$_SITE['registry']['handler']->configuration = $_SITE['config']['handler'];

if ( is_resource( $_SITE['database']['handler'] ) )
     $_SITE['registry']['handler']->Zend_Db_Adapter_Pdo_Mysql = $_SITE['database']['handler'];