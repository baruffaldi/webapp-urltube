<?php
/**
 * Bootstrap: SiteMap ( on Request Update )
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

if ( isset( $_POST['updateSitemap'] ) )
{
     $links = $_SITE['database']['handler']->select( )->from(  'sitemap' )
                                                      ->order( array( 'priority DESC' ) );
     $handle = $links->query( );
     $links = $handle->fetchAll( );
     foreach ( $links as $key => $value ) 
     foreach ( $value as $key1 => $value1 ) if ( $key1 == 'controller' ) $actions[$value1][$links[$key]['action']] = $links[$key];

     $xml = "<!-- generated-on=\"October 1, 2008 1:20 am\" -->
<?xml-stylesheet type=\"text/xsl\" href=\"/public/sldt/sitemap.xsl\"?>
<urlset xsl=\"/public/xslt/sitemap.xsl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
        xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\" 
        xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";

     foreach ( glob( $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                                'views' . DIRECTORY_SEPARATOR . 
                                                'scripts' . DIRECTORY_SEPARATOR . '*' ) as $controller )
     {
          $controller = str_replace( $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                                                'views' . DIRECTORY_SEPARATOR . 
                                                                'scripts' . DIRECTORY_SEPARATOR, NULL, $controller );

          // *** Check if the controller exists on database
          if ( @array_key_exists( $controller, $actions ) !== FALSE )
          {
               foreach( glob( $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                                         'views' . DIRECTORY_SEPARATOR . 
                                                         'scripts' . DIRECTORY_SEPARATOR . 
                                                         $controller . DIRECTORY_SEPARATOR . '*' ) as $action )
               {
                    $patterns[0] = '/'.str_replace( '/', '\\/', $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                                                                           'views' . DIRECTORY_SEPARATOR . 
                                                                                           'scripts' . DIRECTORY_SEPARATOR . 
                                                                                           $controller . DIRECTORY_SEPARATOR ).'/';
                    $patterns[1] = '/.phtml/';
                    $replacements[0] = NULL;
                    $replacements[1] = NULL;
                    $action = preg_replace( $patterns, $replacements, $action );

                    // *** Check if the action exists on database                    
                    if ( @array_key_exists( $action, $actions[$controller] ) ) 
                    {
                        if ( $actions[$controller][$action]['published'] == 'Y' ) 
                        {
                             unset( $url );
                             
                             if ( $action != 'index' ) $url = "$controller/$action";
                             elseif ( $controller != 'index' && $action == 'index' ) $url = $controller;
                             
                             $xml .= "
     <url> 
          <loc>http://{$_SERVER['HTTP_HOST']}:{$_SERVER['SERVER_PORT']}/$url</loc> 
          <lastmod>{$actions[$controller][$action]['lastmod']}</lastmod> 
          <changefreq>{$actions[$controller][$action]['changefreq']}</changefreq> 
          <priority>{$actions[$controller][$action]['priority']}</priority> 
     </url>";
                        }
                        
                        // *** If not present we create a new database entry
                    } else $_SITE['database']['handler']->insert( 'sitemap', array( 'controller' => $controller, 'action' => $action ) );
               }
               // *** If not present we create a new database entry
          } else $_SITE['database']['handler']->insert( 'sitemap', array( 'controller' => $controller ) );
     }
     
     $xml .= "</urlset>";
     
     // *** Save new sitemap
     $sitemap_file = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 
                                                           'public' . DIRECTORY_SEPARATOR . 
                                                           'xml' . DIRECTORY_SEPARATOR . 'sitemap.xml';
     file_put_contents( $sitemap_file, $xml );
     
    // *** GZip new sitemap
    exec( "gunzip -c $sitemap_file > $sitemap_file.gz" );
}