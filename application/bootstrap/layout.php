<?php
/**
 * Bootstrap: MVC
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

$request = array_reverse( explode( '/', $_SERVER['REQUEST_URI'] ) );
//array_pop( $request );
for ( $c = 0; $c <= 1; $c++ )
     $controller = array_pop( $request );

switch ( $controller )
{          
     default:
          $layout = $_SITE['config']['env']['theme'];
          break;
          
     case 'ajax':
     case 'feeds':
          $layout = 'scripts';
          break;
}

if ( file_exists( $_SITE['config']['fs']['path_layout'] . DIRECTORY_SEPARATOR . $layout ) )
     $_SITE['config']['fs']['path_layout'] .= DIRECTORY_SEPARATOR . $layout;
else $_SITE['config']['fs']['path_layout'] .= DIRECTORY_SEPARATOR . 'default';

// CHECK THE SCRIPT NOT THE DIRECTORY
if ( file_exists( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . $layout ) )
     $_SITE['config']['fs']['path_view'] = $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . $layout;
else $_SITE['config']['fs']['path_view'] = $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . 'default';

$_SITE['config']['fs']['path_script'] = $_SITE['config']['fs']['path_view'];

Zend_Layout::startMvc( $_SITE['config']['fs']['path_layout'] );

$_SITE['MVC']['handler']  = Zend_Layout::getMvcInstance();

$_SITE['view']['handler'] = $_SITE['MVC']['handler']->getView();
$_SITE['view']['handler']->setScriptPath( $_SITE['config']['fs']['path_script'] );
$_SITE['view']['handler']->doctype( 'XHTML1_STRICT' );
/*$_SITE['view']['handler']->addHelperPath( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Zend' 
                                                                             . DIRECTORY_SEPARATOR . 'Dojo' 
                                                                             . DIRECTORY_SEPARATOR . 'View' 
                                                                             . DIRECTORY_SEPARATOR . 'Helper' . DIRECTORY_SEPARATOR, 
                                                                             'Zend_Dojo_View_Helper' );*/

//Zend_Dojo_View_Helper_Dojo::setUseDeclarative();
$_SITE['config']['env']['theme'] = $layout;