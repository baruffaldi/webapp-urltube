<?php

class BFCMS_Helper
{
	public function renderTable( $data, $title = NULL, $render_paging = TRUE, $render_title = TRUE, $render_headers = TRUE, $css = array( ), $fields_mapping = array(  ), $hide_fields = array( ), $type = 'table' )
	{
		$css_title = ( array_key_exists( 'title', $css ) ) ? " {$css['title']}" : NULL;
		$css_table = ( array_key_exists( 'table', $css ) ) ? " {$css['table']}" : NULL;
		$css_alter = ( array_key_exists( 'alter', $css ) ) ? " {$css['alter']}" : NULL;
		$css_page  = ( array_key_exists( 'page', $css ) ) ? " {$css['page']}" : NULL;
		$css_tr    = ( array_key_exists( 'tr', $css ) ) ? " {$css['tr']}" : NULL;
		$css_th    = ( array_key_exists( 'th', $css ) ) ? " {$css['th']}" : NULL;
		$css_td    = ( array_key_exists( 'td', $css ) ) ? " {$css['td']}" : NULL;
		$css_dl    = ( array_key_exists( 'dl', $css ) ) ? " {$css['dl']}" : NULL;
		$css_dt    = ( array_key_exists( 'dt', $css ) ) ? " {$css['dt']}" : NULL;
		$css_dd    = ( array_key_exists( 'dd', $css ) ) ? " {$css['dd']}" : NULL;
		
		$cols = count( $data[0] ) - count( $hide_fields );	

		switch( $type )
		{
			default:
			case 'table':
				$html = "<table$css_table>";
				
				if ( $render_title )
				{
					$html .= "<tr>";
					$html .= "<th$css_title colspan=\"$cols\">$title</th>";
					$html .= "</tr>";
				}
				
				if ( $render_headers )
				{
					$html .= "<tr$css_th>";
					$headers = array_keys( $data[0] );
					foreach ( $headers as $header )
					{
						if ( ! is_int( array_search( $header, $hide_fields ) ) )
						{
							$header = ( array_key_exists( $header, $fields_mapping ) ) ? $fields_mapping[$header] : $header;
							$html .= "<th>$header</th>";
						}
					}
					$html .= "</tr>";
				}
				
				foreach ( $data as $k => $d )
				{
					if ( is_float( $k / 2 ) ) $html .= "<tr$css_tr>";
					else $html .= "<tr$css_alter>";
							
					foreach ( $d as $k => $value ) 
					{
						if ( ! is_int( array_search( $k, $hide_fields ) ) ) 
						    if ( substr( $value, ( strlen( $value ) -1 ) ) == '.' ) $html .= '<td>' . substr( $value, 0, -1 ) . '</td>';
						    else $html .= "<td>$value</td>";
					}
					
					$html .= "</tr>";
				}
				
				if ( $render_paging )
				{
					$html .= "<tr$css_page>";
					$html .= "<th colspan=\"$cols\"><a href=\"\" title=\"Previous\">Previous</a> :: <a href=\"\" title=\"Next\">Next</a></th>";
					$html .= "</tr>";
				}
				
				$html .= "</table>";
				break;
				
			case 'list':
				$html = "<dl$css_dl>";
				
				if ( $render_title )
					$html .= "<dt$css_title>$title</dt>";
				
				foreach ( $data as $k => $d )
				{
					if ( is_float( $k / 2 ) ) $html .= "<dd$css_dd>";
					else $html .= "<dd$css_alter>";
					
					foreach ( $d as $k => $value ) 
					{
						if ( ! is_int( array_search( $k, $hide_fields ) ) ) 
						$html .= "$value";
					}
					
					$html .= "</dd>";
				}
				
				if ( $render_paging )
					$html .= "<dt$css_page><a href=\"\" title=\"Previous\">Previous</a> :: <a href=\"\" title=\"Next\">Next</a></dt>";
				
				$html .= "</dl>";
				break;
		}
		
		return $html;
	}

	public function cleanUriGet()
	{
		$return = NULL;
		$first = TRUE;
		
		foreach( $_GET as $key => $value )
		{
			if ( substr( $key, 0, 4 ) != 'page' )
			switch( $key )
			{
				default:
					if ( ! $first ) $return .= "&";
					$return .= $key;
					if ( ! empty( $value ) ) $return .= "=";
					$return .= $value;
					$first = FALSE;
					break;
			}
		}
		
		return $return;
	}
	
	public function getToken( $lenght = 10 )
	{
		while( strlen( $token ) <= $lenght )
		{		
			$subsets[1] = array('min' => 48, 'max' => 57); // ascii numbers
			$subsets[2] = array('min' => 97, 'max' => 122); // ascii lowercase English letters
			//$subsets[3] = array('min' => 65, 'max' => 90); // ascii uppercase English letters
			
			$s = rand( 1, count( $subsets ) );
			
			$ascii_code = rand($subsets[$s]['min'], $subsets[$s]['max']);
			
			// Little hack to keep the scheme "for each one integer, one alpha".
			//if ( is_int( chr( $ascii_code ) ) && is_int( $old_ascii_code ) || ! is_int( chr( $ascii_code ) ) && ! is_int( $old_ascii_code ) ) $ascii_code = NULL;
			// Little hack to avoid double characters
			if ( chr( $ascii_code ) == $old_ascii_code ) $ascii_code = NULL;
			
			if ( ! is_null( $ascii_code ) ) 
			{
				$token .= chr( $ascii_code );
				$old_ascii_code = chr( $ascii_code );
			}
		}
		
		return $token;
	}
}