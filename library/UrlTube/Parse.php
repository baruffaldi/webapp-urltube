<?php

class UrlTube_Parse
{
     public $patterns = array( 'uri' => '^([a-zA-Z]+)\:\/\/([a-zA-Z0-9\-\_\.]+)\.([a-zA-Z]+)((\/.*)|)^',
                               'services' => '^([a-zA-Z]+)\:\/\/(.*)\.(.*)^',
                               'mimetype' => '^.*\/.*^' );
    
    public function setPattern( $type, $pattern )
    {
         $this->patterns[$type] = $pattern;
    }
    
    public function getDataByMimetype( $mimetype )
    {
         @preg_match( $this->patterns['mimetype'], $mimetype, $mime );

         if ( empty( $mime[1] ) )
         {
              $mime = explode( '/', $mimetype );
              return array( 1 => $mime[0], 2 => $mime[1] );
         }

          if ( ! empty( $mime[1] ) ) return $mime;
          else return FALSE;
    }
     
    public function getDataByURI( $uri )
    {
          @preg_match( $this->patterns['uri'], $uri, $host );
          
          if ( ! empty( $host[1] ) ) return $host;
          else return FALSE;
    }
     
     public function getServiceByURI( $url )
     {
          @preg_match( $this->patterns['uri'], $url, $return );
          
          if ( empty( $return[1] ) ) return FALSE;

          $proto  = $return[1];
          $host   = $return[2];
          $tld    = $return[3];
          $domain = array_pop( explode( '.', $host ) );

          if ( stristr( $host, 'maps.' ) ) return 'maps';
          if ( $proto == 'rtsp' || $proto == 'ftp' ) return $proto;
          
          switch( $domain ) 
          {
               default:
                    return 'web';
                    break;
                    
               case 'google':
                         return 'google';
                    break;
                    
               case 'facebook':
                         return 'facebook';
                    break;
                    
               case 'youtube':
                         return 'youtube';
                    break;
                    
               case 'flickr':
                         return 'flickr';
                    break;
          }
     }
     
    public function getProtocolByURI( $uri )
    {
          @preg_match( $this->patterns['uri'], $uri, $host );
          
          if ( ! empty( $host[1] ) ) return $host[1];
          else return FALSE;
    }
     
     public function getCategoryName( $mimetype, $url = NULL )
     {
          global $_SITE;
          
          $mime = $_SITE['UrlTube']['Parser']->getDataByMimetype( $mimetype );
          
          switch( $mime[2] )
          {
               case 'x-flv':
               case 'x-shockwave-flash':
               case 'futuresplash':
               case 'flash':
                    return 'flash';
                    break;
                    
               case 'xml':
               case 'xhtml':
               case 'html':
               case 'wml':
                    if ( ! is_null( $url ) )
                         return $_SITE['UrlTube']['Parser']->getServiceByURI( $url );
                    else return 'web';
                    break;
                    
               case 'pdf':
                    return $mime[2];
                    break;
                    
               case 'quicktime':
                    return $mime[2];
                    break;
          }
          
          switch( $mime[1] )
          {
               default:
                    return 'web';
                    break;
                    
               case 'text':
                    return $mime[1];
                    break;
                    
               case 'audio':
                    return $mime[1];
                    break;
                    
               case 'video':
                    return $mime[1];
                    break;
                    
               case 'image':
                    return $mime[1];
                    break;
          }
     }
     
     public function getImageByMimetype( $mimetype, $url = NULL )
     {
          return UrlTube_Parse::getCategoryName( $mimetype, $url ) . '.png';
     }
     
     public function getMimetypeByCategory( $category )
     {
          switch( $category )
          {
               default:
                    return NULL;
                    break;
                    
               case 'ftp':
               case 'rtsp':
                    return array( '*/*' );
                    break;
                    
               case 'flash':
                    return array( '*/x-flv', '*/x-shockwave-flash', '*/futuresplash', '*/flash' );
                    break;
                    
               case 'web':
                    return array( '*/xml', '*/xhtml', '*/html' );
                    break;
                    
               case 'pdf':
                    return array( '*/pdf' );
                    break;
                    
               case 'quicktime':
                    return array( 'video/quicktime' );
                    break;
                    
               case 'text':
                    return array( 'text/plain' );
                    break;
                    
               case 'audio':
                    return array( 'audio/*' );
                    break;
                    
               case 'video':
                    return array( 'video/*' );
                    break;
                    
               case 'image':
                    return array( 'image/*' );
                    break;
                    
               case 'facebook':
                    return array( '*/xml', '*/xhtml', '*/html' );
                    break;
                    
               case 'flickr':
                    return array( '*/xml', '*/xhtml', '*/html' );
                    break;
                    
               case 'google':
                    return array( '*/xml', '*/xhtml', '*/html' );
                    break;
                    
               case 'maps':
                    return array( '*/xml', '*/xhtml', '*/html' );
                    break;
          }
     }
     
     public function cleanUrlResults( $url )
     {
          foreach( $url as $key => $value )
          {
               switch ( $key )
               {
                    default:
                         $url[$key] = ( substr( $key, strlen( $key ) - 4 ) == 'date' && $value != '0' ) 
                                    ? date( 'y.m.d H:i', $value ) : $value;
          
                         if ( is_null( $value ) ) $url[$key] = __LANG_NOT_AVAILABLE__; 
                         break;
                         
                    case 'ip':
                         $url[$key] = explode( '.', $url[$key] );
                         $url[$key] = "{$url[$key][0]}.x.x.{$url[$key][3]}";
                         break;
                         
                    case 'max_visits':
                         $url[$key] = ( $value != 0 ) ? $value : __LANG_ADDURLTUBE_INFVISITS__;
                         break;
                         
                    case 'visits_counter':
                         $url[$key] = ( $value != 0 ) ? $value : __LANG_NO_ONE_F__;
                         break;
                         
                    case 'title':
                         $url[$key] = $value;
                         //$url[$key] = ( ! empty( $value ) ) ? wordwrap( $value, 25, '<br />', true ) : __LANG_NO_TITLE__;
                         break;
                         
                    case 'description':
                         $url[$key] = $value;
                         //$url[$key] = ( ! empty( $value ) ) ? wordwrap( $value, 40, '<br />', true ) : __LANG_NO_DESCRIPTION__;
                         break;
                         
                    case 'image':
                         $category = UrlTube_Parse::getCategoryName( $url['mimetype'], $url['url'] );
                         if ( $category == 'web' ) $url[$key] = 'http://images.websnapr.com/?url=' . urlencode( $url['url'] ) . '&amp;size=s&amp;nocache=58';
                         elseif ( file_exists( $_SERVER['DOCUMENT_ROOT'] . "public/images/thumbnails/".md5( $url['id_url'] ).".jpg" ) ) $url[$key] = "/public/images/thumbnails/".md5( $url['id_url'] ).".jpg";
                         else $url[$key] = '/public/images/categories/' . UrlTube_Parse::getImageByMimetype( $url['mimetype'] );
                         break;
               }
          }
          
          return $url;
     }
}
$_SITE['UrlTube']['Parser'] = new UrlTube_Parse();