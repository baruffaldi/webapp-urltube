<?php
// TEST: http://www.free-3gp-video.com/download.php?file=backflip-ski-crash.3gp&category=200802
// TEST: http://www.mobileplayground.co.uk/video/Crazy%20Frog.3gp
class UrlTube
{
/* UrlTube Informations needed to put or get it */
     
     /* User input */
     public $url            = NULL;
     public $name           = NULL;
     
     public $public         = 'Y';
     public $masquerade     = 'N';
     public $title          = NULL;
     public $description    = NULL;

     /* Fast Detected Informations */
     public $ip             = NULL;
     public $creation_date  = NULL;
     public $table_url      = NULL;
     public $table_links    = NULL;
     public $table_scans    = NULL;
     
     /* Slow Detected Informations */
     public $net_status      = NULL;
     public $status_code     = NULL;
     public $status_text     = NULL;
     public $mimetype        = NULL;
     public $modified_date   = NULL;
     public $status_update   = NULL;
     public $size            = NULL;
     public $type            = NULL;
     public $mime            = NULL;
     
     public $url_id          = 0;
/* - END - */

     public $max_visits         = 0;
     public $twins_counter      = 0;
     public $twins_max          = 50;
     public $status_update_days = 7;
     
     
     public function UrlTube( )
     {
          global $_SITE;
          
          $this->ip = $_SERVER['REMOTE_ADDR'];

          $this->table_url   = $_SITE['config']['handler']->sil->database->table_url;
          $this->table_links = $_SITE['config']['handler']->sil->database->table_links;
          $this->table_scans = $_SITE['config']['handler']->sil->database->table_scans;
     }
     
     /**
      * This method is used to add a new Url on database
      * 
      * We set all parameters by following with this priority scheme:
      * 
      * -- User Input
      * --- Twins URL
      * ---- Autodetect data
      * ----- Default data
      *
      * @param string $url
      * @param array $options
      * @return array $return
      */
     public function addUrl( $url, $options = array( ) )
     {
          global $_SITE;
          
          foreach ( $options as $key => $value )
          	  if ( ! empty( $options[$key] ) )
              	$this->$key = trim( html_entity_decode( urldecode( $value ) ) );
          
          $this->url = urldecode($url);
               
          if ( ! empty( $this->url ) )
          {
               // *** Check if the specified URL is well-formed
               if ( ! Zend_Uri_UrlTube::check( $this->url ) ) return -5;
               
               $scan = new Zend_UrlTube_Client( $url );
               
               if ( $scan->ping( $url ) == FALSE ) {
                    $this->net_status = 'N';
                    return -5;
               } else $this->net_status = 'Y';
               
               // *** Get twins
               $return['urls'] = UrlTube::getUrlsByUrl( $this->url );

               // *** If the url is not present on database we have to put it into!
               if ( $return['urls'] == FALSE ) {
                    $return['url_query'] = $scan->scanStatusResult( $this->url );
                    if ( $return['url_query'] == FALSE ) return -2;
                    
                    $this->net_status     = trim( $return['url_query']['net_status'] );
                    $this->status_update  = trim( $return['url_query']['update'] );
                    $this->status_code    = trim( $return['url_query']['code'] );
                    $this->status_text    = trim( $return['url_query']['text'] );
          
                    $this->modified_date  = ( ! is_null( $return['url_query']['date'] ) )     ? trim( $return['url_query']['date'] )     : NULL;
                    $this->size          = ( ! is_null( $return['url_query']['size'] ) )     ? trim( $return['url_query']['size'] )     : NULL;
                    $this->mimetype      = ( ! is_null( $return['url_query']['mimetype'] ) ) ? trim( $return['url_query']['mimetype'] ) : NULL;
                    $this->mime          = ( ! is_null( $return['url_query']['mime'] ) )     ? trim( $return['url_query']['mime'] )     : NULL;
                    $this->type          = ( ! is_null( $return['url_query']['type'] ) )     ? trim( $return['url_query']['type'] )     : NULL;                         
               }
               else 
               {
                    // *** Check if the URL is banned
                    foreach( $return['urls'] as $sil )
                         if ( $sil['public'] == 'N' ) return -3;
                    
                    // *** Check if the URL has reached its Url availability limit.
                    $this->twins_counter = count( $return['urls'] );
                    if ( $this->twins_counter >= $this->twins_max ) return -4;

                    
                    if ( $return['urls'][0]['status_update'] <= ( time( ) - ( 86400 * $this->status_update_days ) )
                      || $return['urls'][0]['public'] == 'I' ) {
                           
                           $result = $scan->scanStatus( );
                         $return['url_query'] = $scan->scanStatusResult( $this->url, $result );
                         
                         $this->net_status     = trim( $return['url_query']['net_status'] );
                         $this->status_update  = trim( $return['url_query']['update'] );
                         $this->status_code    = trim( $return['url_query']['code'] );
                         $this->status_text    = trim( $return['url_query']['text'] );
               
                         $this->modified_date  = ( ! is_null( $return['url_query']['date'] ) )     ? trim( $return['url_query']['date'] )     : NULL;
                         $this->size          = ( ! is_null( $return['url_query']['size'] ) )     ? trim( $return['url_query']['size'] )     : NULL;
                         $this->mimetype      = ( ! is_null( $return['url_query']['mimetype'] ) ) ? trim( $return['url_query']['mimetype'] ) : NULL;
                         $this->mime          = ( ! is_null( $return['url_query']['mime'] ) )     ? trim( $return['url_query']['mime'] )     : NULL;
                         $this->type          = ( ! is_null( $return['url_query']['type'] ) )     ? trim( $return['url_query']['type'] )     : NULL;
                         
                       if ( $return['url_query'] == FALSE ) return -2;
                    } else {
                         $this->net_status     = trim( $return['urls'][0]['net_status'] );
                         $this->status_update  = trim( $return['urls'][0]['status_update'] );
                         $this->status_code    = trim( $return['urls'][0]['status'] );
                         $this->status_text    = trim( $return['urls'][0]['status_text'] );
               
                         $this->modified_date = ( ! is_null( $return['urls'][0]['modified_date'] ) )     ? trim( $return['urls'][0]['date'] )     : NULL;
                         $this->size          = ( ! is_null( $return['urls'][0]['size'] ) )     ? trim( $return['urls'][0]['size'] )     : NULL;
                         $this->mimetype      = ( ! is_null( $return['urls'][0]['mimetype'] ) ) ? trim( $return['urls'][0]['mimetype'] ) : NULL;
                         $mime = $_SITE['UrlTube']['Parser']->getDataByMimetype( $this->mimetype );
                         $this->mime          = $mime[1];
                         $this->type          = $mime[2];
                    }
               }

               
               /* *** OLD LINKS AND SCAN SECTION ENDS *** */
               /* *** ******************************* *** */
               $return['url_id'] = $this->getURLIdByURL( $this->url );
               $this->id_url = $return['url_id'] = $return['url_id'][0]['id_url'];
               
               /**
                * 
                * If the user has not filled the sil field, we have to generate
                * a token that not already exists on database.
                *
                */
               if ( ! is_null( $this->name ) )
               {
                    if ( $this->getUrlIdByname( $this->name, TRUE ) )
	                    while ( $this->getUrlIdByname( $this->name, TRUE ) )
	                         $this->name = BFCMS_Helper::getToken( );
                    else $this->name = trim( strtolower( $this->name ) );
               } else {
                    $this->name = BFCMS_Helper::getToken( );
                    while ( $this->getUrlIdByname( $this->name, TRUE ) )
                         $this->name = BFCMS_Helper::getToken( );
               }
                              

               // *** Input values
               $this->title       = ( is_null( $this->title ) )       ? trim( html_entity_decode( urldecode( $options['add_title'] )       ) ): $this->title;
               $this->description = ( is_null( $this->description ) ) ? trim( html_entity_decode( urldecode( $options['add_description'] ) ) ): $this->description;

               $this->creation_date = time( );
               
               foreach( $this as $key => $value )
                 $return[$key] = $value;
               
               $return['query'] = $_SITE['database']['handler']->insert( $this->table_links, array( 'ip' => $this->ip, 
                                                                                                     'creation_date' => $this->creation_date, 
                                                                                                     'id_url' => $this->id_url, 
                                                                                                     'name' => $this->name, 
                                                                                                     'title' => $this->title, 
                                                                                                     'description' => $this->description, 
                                                                                                     'public' => $this->public, 
                                                                                                     'max_visits' => $this->max_visits, 
                                                                                                     'masquerade' => $this->masquerade ) );
            	$return['link_id'] = $_SITE['database']['handler']->lastInsertId();
            
                 // *** If is lesser than 1100k, let's generate on request the thumbnail
            if ( $this->mime == 'image' && $this->size <= 1100000 )
                 $this->backgroundProcess( 'php ' . $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 'cronscripts' . DIRECTORY_SEPARATOR . "scanUrlById.php {$_SITE['config']['env']['type']} 0 {$this->id_url}" );
            
               return $return;
          } else {
               // No URL specified
               return -1;
          }
     }
     /* href="backend.php?return=time" 
   3     onclick="new Ajax.Updater('testdiv', 'backend.php?return=time',
   4     {asynchronous:true, evalScripts:true }); return false;"*/
     public function foregroundProcess($Command, $Priority = 0)
     {
        if($Priority)
            $PID = shell_exec("nice -n $Priority $Command");
        else
            $PID = shell_exec("$Command");
        return $PID;
     }
     
     private function backgroundProcess($Command, $Priority = 0)
     {
        if($Priority)
            $PID = shell_exec("nohup nice -n $Priority $Command 2> /dev/null & echo $!");
        else
            $PID = shell_exec("nohup $Command 2> /dev/null & echo $!");
        return $PID;
     }
     
     public function isbackgroundProcessRunning($PID)
     {
        exec("ps $PID", $ProcessState);
        return(count($ProcessState) >= 2);
     }
     
     public function getUrlsToUpdate( $urlColumns = NULL, $scanColumns = NULL, $order = NULL, $public = 'Y', $mimetypes = NULL, $service = NULL, $protocol = NULL, $paging = NULL, $hideNoTitle = FALSE, $where = NULL )
     {
          global $_SITE;
          
          //_dump(func_get_args());

          if ( ! is_null( $_SITE['config']['handler'] ) )
          {
               $services = explode( ',', $_SITE['config']['handler']->services );
               
               foreach( $services as $k => $v ) $services[$k] = trim( $v );
               
               if ( ! in_array( $service, $services ) ) $service = NULL;
          }

          if ( ! is_null( $_SITE['config']['handler'] ) )
          {
               $protocols = explode( ',', $_SITE['config']['handler']->protocols );
               
               foreach( $protocols as $k => $v ) $protocols[$k] = trim( $v );
               
               if ( ! in_array( $proto, $protocols ) ) $protocol = NULL;
          }

          $query = $_SITE['database']['handler']->select( )
                                               ->from( array( 'url' => $_SITE['config']['handler']->sil->database->table_url ), $urlColumns )
                                               ->joinLeft( array( 'scans' => $_SITE['config']['handler']->sil->database->table_scans ), '( scans.id_url = url.id_url )', $scanColumns );
          
          if ( ! is_null( $order ) ) $query->order( array_merge( $order, array( 'scans.scan_date ASC' ) ) );
          else $query->order( array( 'scans.scan_date ASC', 'url.creation_date ASC' ) );
          
          if ( ! is_null( $mimetypes ) )
          {
               $c = 0;
               $mimetypeWhere = NULL;
               
               foreach ( $mimetypes as $mimetype )
               {
                    if ( $c == count( $mimetypes ) - 1 ) {
                         $c++;
                         $mimetypeWhere .= 'url.mimetype LIKE \'' . str_replace( '*', '%', $mimetype ) . '\'';
                    } else {
                         $c++;
                         $mimetypeWhere .= 'url.mimetype LIKE \'' . str_replace( '*', '%', $mimetype ) . '\' OR ';
                    }               
               }
               
               $query->where( $mimetypeWhere );
          }
          
          if ( ! is_null( $protocol ) )
               $query->where( "url.url LIKE ?", "$protocol://%" );
          
          $query->where( "url.public = 'Y'");

          if ( ! is_null( $where ) )
               foreach ( $where as $field => $value )
                    $query->where( "$field", $value );
          
          if ( $hideNoTitle ) 
          {
               $query->where( "scans.title != '' OR scans.description != ''" );
          } else {
               //$query->where( "scans.title = '' OR scans.description = ''" );
          }

          if ( ! is_null( $paging ) )
          {
               $query->limit( $paging['limit'], $paging['start'] );
               if ( $service == 'blacklist' ) $query->limit( $paging['limit'], $paging['start'] );
          } 

          $handle = $query->query( );
          $urls   = $handle->fetchAll( );

          $urls[0]['total'] = (int) count($urls);
          
          //var_dump( $query->__toString() );
//          var_dump( is_array( $urls[0] ) );
          //_dump($mimetypes);_dump($service);_dump($protocol);_dump($paging);

          if ( is_array( $urls[0] ) ) return $urls;
          else return FALSE;
     }
     
     public function getUrls( $linkColumns = NULL, $urlColumns = NULL, $scanColumns = NULL, $order = NULL, $public = 'Y', $mimetypes = NULL, $service = NULL, $protocol = NULL, $paging = NULL, $hideNoTitle = TRUE, $where = NULL )
     {
          global $_SITE;
          
          //_dump(func_get_args());

          if ( ! is_null( $_SITE['config']['handler'] ) )
          {
               $services = explode( ',', $_SITE['config']['handler']->services );
               
               foreach( $services as $k => $v ) $services[$k] = trim( $v );
               
               if ( ! in_array( $service, $services ) ) $service = NULL;
          }

          if ( ! is_null( $_SITE['config']['handler'] ) )
          {
               $protocols = explode( ',', $_SITE['config']['handler']->protocols );
               
               foreach( $protocols as $k => $v ) $protocols[$k] = trim( $v );
               
               if ( ! in_array( $proto, $protocols ) ) $protocol = NULL;
          }

          $query = $_SITE['database']['handler']->select( )
                                                ->from( array( 'links' => $_SITE['config']['handler']->sil->database->table_links ), $linkColumns )
                                                ->join( array( 'url' => $_SITE['config']['handler']->sil->database->table_url ), 'links.id_url = url.id_url', $urlColumns )
                                                ->joinLeft( array( 'scans' => $_SITE['config']['handler']->sil->database->table_scans ), '( scans.id_url = links.id_url )', $scanColumns )
                                                ->columns( array( 'title' => 'IF(links.title!=\'\',links.title,scans.title)', 'description' => 'IF(links.description!=\'\',links.description,scans.description)' ) );
          
          if ( ! is_null( $order ) ) $query->order( $order );
          else $query->order( array( 'links.creation_date DESC', 'links.visits_counter DESC' ) );
          
          $totalQuery = $_SITE['database']['handler']->select( )
                                                     ->from( array( 'links' => $_SITE['config']['handler']->sil->database->table_links ), array( 'id' ) )
                                                     ->join( array( 'url' => $_SITE['config']['handler']->sil->database->table_url ), 'links.id_url = url.id_url', $urlColumns )
                                                     ->joinLeft( array( 'scans' => $_SITE['config']['handler']->sil->database->table_scans ), '( scans.id_url = links.id_url )', $scanColumns )
                                                     ->columns( array( 'title' => 'IF(links.title!=\'\',links.title,scans.title)', 'description' => 'IF(links.description!=\'\',links.description,scans.description)' ) )
                                                     ->order( array( 'links.creation_date DESC', 'links.visits_counter DESC' ) );
          	   
          if ( ! is_null( $mimetypes ) )
          {
               $c = 0;
               $mimetypeWhere = NULL;
               
               foreach ( $mimetypes as $mimetype )
               {
                    if ( $c == count( $mimetypes ) - 1 ) {
                         $mimetypeWhere .= 'url.mimetype LIKE \'' . str_replace( '*', '%', $mimetype ) . '\'';
                    } else {
                        $c++;
                    	$mimetypeWhere .= 'url.mimetype LIKE \'' . str_replace( '*', '%', $mimetype ) . '\' OR ';
                    }
               }
               
               $query->where( $mimetypeWhere );
               $totalQuery->where( $mimetypeWhere );     
          }
          
          if ( ! is_null( $protocol ) )
          {
               $query->where( "url.url LIKE ?", "$protocol://%" );
               $totalQuery->where( "url.url LIKE ?", "$protocol://%" );
          }
          
          $query->where( "links.public = ?", $public );
          $totalQuery->where( "links.public = ?", $public );

          if ( ! is_null( $where ) )
               foreach ( $where as $field => $value )
                    $query->where( "$field", $value );
          
          if ( $hideNoTitle ) 
          {
               $query->having( "title != '' OR title != ''" );
               $totalQuery->having( "title != ? OR title != ''", '' );
          }

          if ( ! is_null( $paging ) )
               $query->limit( $paging['limit'], $paging['start'] );

          $handle = $totalQuery->query( );
          $total  = $handle->fetchAll( );
          
          $handle = $query->query( );
          $urls   = $handle->fetchAll( );

          $urls[0]['total'] = (int) count($total);

          //var_dump($query->__toString());
//          var_dump( is_array( $urls[0] ) );
          //_dump($mimetypes);_dump($service);_dump($protocol);_dump($paging);

          if ( is_array( $urls[0] ) ) return $urls;
          else return FALSE;
     }
     
     public function getUrl( $linkColumns = NULL, $urlColumns = NULL, $scanColumns = NULL, $order = NULL, $public = 'Y', $where = NULL, $mimetypes = NULL, $service = NULL, $protocol = NULL )
     {
          global $_SITE;

          if ( ! is_null( $_SITE['config']['handler'] ) )
          {
               $services = explode( ',', $_SITE['config']['handler']->services );
               
               foreach( $services as $k => $v ) $services[$k] = trim( $v );
               
               if ( ! in_array( $service, $services ) ) $service = NULL;
          }

          if ( ! is_null( $_SITE['config']['handler'] ) )
          {
               $protocols = explode( ',', $_SITE['config']['handler']->protocols );
               
               foreach( $protocols as $k => $v ) $protocols[$k] = trim( $v );
               
               if ( ! in_array( $proto, $protocols ) ) $protocol = NULL;
          }

          $query = $_SITE['database']['handler']->select( )
                                              ->from( array( 'links' => $_SITE['config']['handler']->sil->database->table_links ), $linkColumns )
                                              ->join( array( 'url' => $_SITE['config']['handler']->sil->database->table_url ), 'links.id_url = url.id_url', $urlColumns )
                                              ->joinLeft( array( 'scans' => $_SITE['config']['handler']->sil->database->table_scans ), '( scans.id_url = links.id_url )', $scanColumns )
                                              ->columns( array( 'title' => 'IF(links.title!=\'\',links.title,scans.title)', 'description' => 'IF(links.description!=\'\',links.description,scans.description)' ) );
          
          if ( ! is_null( $order ) ) $query->order( array_merge( $order, array( 'links.creation_date DESC' ) ) );
          else $query->order( array( 'links.creation_date DESC', 'links.visits_counter DESC' ) );
          
          if ( ! is_null( $mimetypes ) )
          {
               $c = 0;
               $mimetypeWhere = NULL;
               
               foreach ( $mimetypes as $mimetype )
               {
                    if ( $c == count( $mimetypes ) - 1 ) {
                         $c++;
                         $mimetypeWhere .= 'url.mimetype LIKE \'' . str_replace( '*', '%', $mimetype ) . '\'';
                    } else {
                         $c++;
                         $mimetypeWhere .= 'url.mimetype LIKE \'' . str_replace( '*', '%', $mimetype ) . '\' OR ';
                    }               
               }
               
               $query->where( $mimetypeWhere );     
          }
          
          if ( ! is_null( $protocol ) )
               $query->where( "url.url LIKE ?", "$protocol://%" );
          
          if ( $public == 'Y' )
          	$query->where( "links.public = ?", $public );
          
          if ( ! is_null( $where ) )     
               foreach ( $where as $field => $value )
                    $query->where( "$field", $value );     

          $query->limit( 1 );
          $handle = $query->query( );
          $url   = $handle->fetchAll( );

          //_dump($mimetypes);_dump($service);_dump($protocol);_dump($paging);

          if ( ! empty( $url[0]['url'] ) ) return $url[0];
          else return FALSE;
     }
     
     public function getUrlsByUrl( $url = NULL )
     {
          global $_SITE;
          
          if ( ! is_null( $url ) )
          {
               $linksColumns = array( 'id', 'visits_counter', 'max_visits', 'name' );
               $urlColumns = array( 'id_url', 'url', 'mimetype' );
               $scansColumns = array( 'scan_date' );
               $order = array( 'links.visits_counter DESC', 'links.creation_date DESC' );
               $public = 'Y';
               $where = array( 'url.url = ?' => $url );
               $mimetype = NULL;
               $service = NULL;
               $protocol = NULL;
               $paging = NULL;
               
               $urls = UrlTube::getUrls( $linksColumns, $urlColumns, $scansColumns, $order, $public, $mimetype, $service, $protocol, $paging, FALSE, $where );
               
               if ( is_array( $urls[0] ) ) return $urls;
               else return FALSE;     
          } else return NULL;
     }
     
     public function getSilBySilDomain( $name )
     {
          global $_SITE;
          
          if ( ! is_null( $name ) )
          {
               $sil = $_SITE['database']['handler']->select( )
                                                  ->from( array( 'links' => $_SITE['config']['handler']->sil->database->table_links ) )
                                                  ->joinLeft( array( 'scans' => $_SITE['config']['handler']->sil->database->table_scans ), '( scans.id_url = links.id_url )' )
                                                  ->join( array( 'url' => $_SITE['config']['handler']->sil->database->table_url ), 'links.id_url = url.id_url' )
                                                  ->columns( array( 'title' => 'IF(links.title!=\'\',links.title,scans.title)', 'description' => 'IF(links.description!=\'\',links.description,scans.description)', 's_title' => 'scans.title', 's_description' => 'scans.description' ) )
                                                   ->where( "links.name = ?", $name )
                                                   ->where( "links.public != ?", 'H' )
                                                   ->limit( 1 );

               $handle = $sil->query( );
               $sil    = $handle->fetchAll( );
               
               if ( ! empty( $sil[0]['name'] ) ) return $sil[0];
               else return FALSE;
          } else return NULL;
     }
     
     public function getUrlIdByname( $name = NULL, $hidden = FALSE )
     {
          global $_SITE;
          
          if ( ! is_null( $name ) )
          {
               $sil = $_SITE['database']['handler']->select( )
                                                   ->from( $this->table_links, array( 'id' ) )
                                                   ->where( "name = ?", $name )
                                                   //->where( "public = ?", ( $hidden ) ? 'X' : 'H' )
                                                   ->order( array( 'id DESC' ) );
               $handle = $sil->query( );
               $sil    = $handle->fetchAll( );

               if ( ! empty( $sil[0]['id'] ) ) return $sil[0]['id'];
               else return FALSE;
          } else return NULL;
     }
     
     public function getURLIdByURL( $url )
     {
          global $_SITE;
          
          if ( ! is_null( $url ) )
          {
               $urls = $_SITE['database']['handler']->select( )
                                                    ->from( $_SITE['config']['handler']->sil->database->table_url, array( 'id_url' ) )
                                                    ->where( "url = ?", $url );
               $handle = $urls->query( );
               return $handle->fetchAll( );
          } else return FALSE;
     }
     /*
     public function redirect( $url )
     {
          // @todo Controllo status dell'url ( prima dalla data sul db per last_status_update a meno di 3600*7 altrimenti recheck and update last_status_update ) in caso di 404 o 500 Restituire una pagina di errore interna nostra ( courtesy page )
          if ( $sil['status'] != '200' ) return -1;
          // @todo Controllare se � la un multiplo di 5 per visualizzare la pubblicita' con timer per redirect
          if ( ( $sil['counterVisit'] / 10 ) != '200' ) return 2;
          // @todo Log del visionamento
          // @todo Check se da visualizzare l'introduzione
          // @todo Check se � da visualizzare tramite iFrame o redirect
          // @todo Sistema iFrame/redirect compatibile per old mobile devices
     }*/
}