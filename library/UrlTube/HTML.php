<?php


class UrlTube_HTML
{
     public function getHTMLContactUsForm( $recaptcha, $result = NULL )
     {
     	if ( ! is_null( $result ) ) $result = '<dt>Warning: '.$result->getErrorCode().'</dt>';
     	?><form action="" method="POST">
                                             <dl id="contact_form"><?=$result?>
                                                  <dt style="text-align: right;"><i>* = <?=__LANG_NEEDED__?></i></dt>
                                                  <dt style="font-weight: normal;">&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=__LANG_NAME__?></strong></dt><dd>
                                                  <input type="text" name="name" size="64" maxlength="64" /></dd>
                                                  <dt style="font-weight: normal;">&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=__LANG_SURNAME__?></strong></dt><dd>
                                                  <input type="text" name="surname" size="64" maxlength="64" /></dd> 
                                                  <dt style="font-weight: normal;">*&nbsp;&nbsp;<strong><?=__LANG_EMAIL__?></strong></dt><dd>
                                                  <input type="text" name="email" size="64" maxlength="256" /></dd> 
                                                  <dt style="font-weight: normal;">*&nbsp;&nbsp;<strong><?=__LANG_MESSAGE__?></strong></dt><dd>
                                                  <textarea name="message">&nbsp;</textarea></dd>
                                                  <dt style="text-align: right;width: 260px;float:right;padding-right: 130px;"><?=$recaptcha->getHTML();?></dt>
                                                  <dt style="clear:both;text-align: right;padding-right: 220px;"><input id="addUrlSubmit" type="submit" value="<?=__LANG_SEND__?>" /></dt>
                                             </dl></form>
         <?
     }
	
     public function getAddUrlResult( $Url, $error = 0, $addDetails = TRUE, $html = TRUE )
     {
          global $_SITE;
          
          	if ( ! is_array( $error ) ) {
		  		
          		
          	if ( $addDetails )
          		 print UrlTube_HTML::getHTMLCourtesyPageByErrorCode( $error, array( 'status_code' => $Url['status_code'], 'status_text' => $Url['status_text'] ), FALSE );
          	else print '" ' . UrlTube_HTML::getHTMLCourtesyPageByErrorCode( $error, array( 'status_code' => $Url['status_code'], 'status_text' => $Url['status_text'] ), FALSE ) . '"';
          	
          		if ($html) print '<br /><a href="/" alt="'.__LANG_GO_BACK__.'"><h2>'.__LANG_GO_BACK__.'</h2></a>';
     		}
          else
          {
          	if ( $addDetails )
          	{
          
								          print '
								          <div style="margin: 10px 10px 10px 10px; padding: 30px 30px 30px 30px;">
								          <p id="entry_content">';
                                        print __LANG_ADDURL_SUCCESS__;?><br />
                                        <br />
                                        <dl>
                                        <dt>URL:</dt><dd><?=substr( $Url->url, 0, 20 )?> ...<br />
                                        <dt>UrlTube TinyURL:</dt><dd>[<a href="http://<?=$Url->name?>.<?=$_SITE['config']['handler']->redirect->domain?>" title="http://<?=$Url->name?>.<?=$_SITE['config']['handler']->redirect->domain?>">link</a>] http : // <?=$Url->name?> . <?=str_replace( '.', ' . ', $_SITE['config']['handler']->redirect->domain )?></dd>
                                        <dt>UrlTube Content Page:</dt><dd>[<a href="http://www.<?=$_SITE['config']['handler']->redirect->domain?>/browse/url?<?=$Url->name?>" title="http://<?=$Url->name?>.<?=$_SITE['config']['handler']->redirect->domain?>">link</a>] http : // www . <?=str_replace( '.', ' . ', $_SITE['config']['handler']->redirect->domain )?> / browse / url ? <?=$Url->name?></dd><dd>&nbsp;</dd>
                                        
          <? /* SE C'E' CORRISPONDENZA CON L'INPUT O CON UNA PRECEDENTE SCANSIONE PRINTARE:
                                        
                                        <dt>Titolo:</dt><dd><?=$Url->title?></dd>
                                        <dt>Descrizione:</dt><dd><?=$Url->description?></dd><dd>&nbsp;</dd>
          */?>
                                        <dt><?=__LANG_INDEXING__?></dt><dd><?=( $Url->active = 'Y' ) ? 'pubblico' : 'privato'?></dd>
                                        <dt><?=__LANG_URL_LIFETIME__?></dt><dd><?=( $Url->max_visits == 0 ) ? __LANG_ADDURLTUBE_INFVISITS__ : $Url->max_visits?></dd><dd>&nbsp;</dd>
                                        <dt><?=__LANG_CREATION_DATE__?></dt><dd><?=date( 'd.m.Y H:i:s', $Url->creation_date )?></dd>
                                        <dt><?=__LANG_CREATOR_IP__?></dt><dd><?=$Url->ip?></dd>
                                        </dl>
                                        </p><?php

                                        if ( count($error['urls']) >= 2 )
                                        {
                                             print "<div style=\"border: 3px solid #A0A0E0; margin: 10px 10px 10px 10px; padding: 30px 30px 30px 30px;\"><dl><dt>This URL already exists as Url, this is the list of it:</dt>";
                                             foreach ( $error['urls'] as $twinSil )
                                                  print "<dd>[{$twinSil['visits_counter']} visits] http : // <a href=\"http://{$twinSil['name']}.{$_SITE['config']['handler']->redirect->domain}\" title=\"http://{$twinSil['name']}{$_SITE['config']['handler']->redirect->domain}\">{$twinSil['name']} . " . str_replace( '.', ' . ', $_SITE['config']['handler']->redirect->domain ) . "</a>";
                                             print "</dl></div>";
                                        }
          print '</div>';
          	} else { print Zend_Json::encode( array( "OK", "http://{$Url['name']}.{$_SITE['config']['handler']->redirect->domain}" ) ); }
          }
          
     }
     
     public function getHTMLCourtesyPageByErrorCode( $errorCode, $details = array( ), $html = TRUE )
     {
          global $_SITE;
          
          if ($html)
          $return .= '
          <div style="margin: 10px 10px 10px 10px; padding: 30px 30px 30px 30px;">
          <p id="entry_content">';
          if ( ! is_integer( $errorCode ) ) return FALSE;
          switch ( $errorCode )
          {
               default:
                    break;
                    
               case -1:
                    if ($html) $return .= '<h2>';
                    $return .= __LANG_NO_URL_SPECIFIED__;
                    if ($html) $return .= '</h2>';
                    break;
                    
               case -2:
                    if ($html) $return .= '<h2>';
                    $return .= __LANG_OFFLINE_URL_SPECIFIED__;
                    if ($html) $return .= '</h2>';
                    if ($html) $return .= '<p>';
                    if ($html) $return .= __LANG_OFFLINE_URL_SPECIFIED_DESC__;
                    if ($html) $return .= '<br />
                              <dl><dt>';
                    $return .= __LANG_DETAILS__;
                    if ($html) $return .= ':</dt><dd>';
                    if ($html) $return .= '<br /><b>';
                    $return .= $details['statusCode'];
                    if ($html) $return .= '</b> <i>';
                    $return .= $details['statusText'];
                    if ($html) $return .= '</i></dd></dl>';
                    break;
                    
               case -3:
                    if ($html) $return .= '<h2>';
                    $return .= __LANG_BANNED_URL_SPECIFIED__;
                    if ($html) $return .= '</h2>';
                    break;
                    
               case -4:
                    if ($html) $return .= '<h2>';
                    $return .= __LANG_LIMITEXC_URL_SPECIFIED__;
                    if ($html) $return .= '</h2>';
                    if ($html) $return .= '<div style="width: 700px;border: 3px solid #A0A0E0; margin: 10px 10px 10px 10px; padding: 30px 30px 30px 30px;"><dl><dt>';
                    if ($html) $return .= __LANG_URL_LIST__;
                    if ($html) $return .= '</dt></dl>';
                                   
                    $linksColumns = array( 'id', 'visits_counter', 'max_visits', 'name' );
                    $urlColumns = array( 'id_url', 'url', 'mimetype' );
                    $scansColumns = NULL;
                    $order = array( 'creation_date DESC' );
                    $active = 'Y';
                    $where = array( 'url.url' => $_POST['add_url'] );
                    $mimetype = NULL;
                    $service = NULL;
                    $protocol = NULL;
                    $paging = NULL;
                    
                    $twinUrls = UrlTube::getUrls( $linksColumns, $urlColumns, $scansColumns, $order, $active, $mimetype, $service, $protocol, $paging, FALSE, $where );

                    foreach ( $twinUrls as $id => $twinSil )
                         if ($html) $return .= "<dd>[{$twinSil['visits_counter']} ".__LANG_VISITS__."] http : // <a href=\"http://{$twinSil['name']}.{$_SITE['config']['handler']->redirect->domain}\" title=\"{$twinSil['title']}\">{$twinSil['name']}</a> . " . str_replace( '.', ' . ', $_SITE['config']['handler']->redirect->domain) . "</dd>";
                                   
                    if ($html) $return .= '</dl></div></p>';
                    break;
                    
               case -5:
                    if ($html) $return .= '<h2>';
                    $return .= __LANG_INVALID_URL_SPECIFIED__;
                    if ($html) $return .= '</h2>';
                    if ($html) $return .= '<p>';
                    if ($html) $return .= __LANG_INVALID_URL_SPECIFIED_DESC__;
                    if ($html) $return .= '</p>';
                    break;
                    
               case -6:
                    if ($html) $return .= '<h2>';
                    $return .= __LANG_DUPLICATE_URL_SPECIFIED__;
                    if ($html) $return .= '</h2>';
                    if ($html) $return .= '<p>';
                    if ($html) $return .= __LANG_DUPLICATE_URL_SPECIFIED_DESC__;
                    if ($html) $return .= '</p>';
                    break;
          } 
          if ($html) $return .= '</div>';
          return $return;
     }
     
     public function getHTMLUrlDetail( $url )
     {
          global $_SITE;

          $return = "
          
          <div id=\"page\">
          <div id=\"content\">
               <div class=\"post\">
                    <h2 class=\"title\"><a style=\"color: #A0A0E0;\" href=\"\" title=\"".__LANG_DETAILS__.": {$url['title']}\">".__LANG_DETAILS__.": {$url['title']}</a></h2>
                    <p class=\"meta\"><small><b>http : // <a href=\"http://{$url['name']}.{$_SITE['config']['handler']->redirect->domain}\" title=\"{$url['title']}\">{$url['name']}</a> . " . str_replace( '.', ' . ', $_SITE['config']['handler']->redirect->domain ) . "</b></small></p>
                    <div class=\"entry\">
                         <div id=\"content_description\" style=\"text-align: left;float:left;\"><a href=\"http://{$url['name']}.{$_SITE['config']['handler']->redirect->domain}?noPreview\" title=\"{$url['title']}\"><img style=\"width: 300px;\" src=\"{$url['image']}\" alt=\"Url ".__LANG_THUMBNAIL__."\" /></a><br />
                         <h2 style=\"text-align: center;\">".__LANG_DESCRIPTION__."</h2><i>{$url['description']}</i></div>";
          
          if ( $url['scan_date'] == __LANG_NOT_AVAILABLE__ )
          {
               $return .= "
                         <table id=\"url-details\">
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_SIZE__."</b><br />{$url['size']}</td><td>".__LANG_URL_NOT_SCANNED__."</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_VISITS__."</b><br />{$url['visits_counter']}</td><td><b>".__LANG_URL_LIFETIME__."</b><br />{$url['max_visits']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_CREATOR_IP__."</b><br />{$url['ip']}</td><td><b>".__LANG_ROBOT_LAST_UPDATE__."</b><br />{$url['scan_date']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_CREATION_DATE__."</b><br />{$url['creation_date']}</td><td><b>".__LANG_CONTENT_UPDATE__."</b><br />{$url['modified_date']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_STATUS_SCAN_DATE__."</b><br />{$url['status_update']}</td><td></td></tr>
                         </table>";
          } else {
               
               $return .= "<table id=\"url-details\">
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_RESOLUTION__."</b><br />{$url['resolution']}</td><td><b>".__LANG_CODEC__."</b><br />{$url['codec']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_CHANNELS__."</b><br />{$url['channels']}</td><td><b>".__LANG_X_RATING__."</b><br />( ".array_pop(array_reverse(explode('.',$url['xrate'])))."% ) <img src='http://i18n.xfce.org/stats/images/bar-red.png' style='border-left: 1px solid #A0A0E0;border-top: 1px solid #A0A0E0;border-bottom: 1px solid #A0A0E0;vertical-align: middle;height: 20px;width:".( (int) array_pop(array_reverse(explode('.',$url['xrate']))) * 2 )."px;' /><img src='http://i18n.xfce.org/stats/images/bar-green.png' style='border-right: 1px solid #A0A0E0;border-top: 1px solid #A0A0E0;border-bottom: 1px solid #A0A0E0;vertical-align: middle;height: 20px;width:".( 200 - ( (int) array_pop(array_reverse(explode('.',$url['xrate']))) * 2 ) ) ."px;' /></td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_SIZE__."</b><br />{$url['size']}</td><td><b>Bits</b><br />{$url['bits']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_VISITS__."</b><br />{$url['visits_counter']}</td><td><b>".__LANG_URL_LIFETIME__."</b><br />{$url['max_visits']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_CREATOR_IP__."</b><br />{$url['ip']}</td><td><b>".__LANG_ROBOT_LAST_UPDATE__."</b><br />{$url['scan_date']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_CREATION_DATE__."</b><br />{$url['creation_date']}</td><td><b>".__LANG_CONTENT_UPDATE__."</b><br />{$url['modified_date']}</td></tr>
                              <tr><td style=\"text-align: right;\" class=\"align-right\"><b>".__LANG_STATUS_SCAN_DATE__."</b><br />{$url['status_update']}</td><td><b>Tags</b></b><br />".wordwrap($url['tags'], 30, '<br />', true)."</td></tr>
                         </table>";
               
          }
          
          $return .= "</div>
               </div>
          </div>
     </div>";
          
          return $return;
     }
     
     public function getHTMLCategoryWeb( $urls, $limit = 9 )
     {
          global $_SITE;
          
          foreach ( $urls as $id => $v )
               $urls[$id] = UrlTube_Parse::cleanUrlResults( $v );

          print "<table id=\"category_listing\"><tr>";
          for ( $c = 0; $c <= $limit; $c++ )
          {
               if ( !empty( $urls[$c] ) )
               {
                    print '<tr><td>';
                  $mime  = $_SITE['UrlTube']['Parser']->getDataByMimetype( $urls[$c]['mimetype'] );
                    $url   = ( $mime[1] == 'text' ) ? $urls[$c]['url'] : NULL;
                    $proto = $_SITE['UrlTube']['Parser']->getDataByURI( $urls[$c]['url'] );
                    $proto = $proto[1];
                    $proto = ( $proto == 'ftp' ) ? "<img style=\"width: 27px;vertical-align: middle;\" src=\"/public/images/categories/small.ftp.png\" alt=\"-$proto-\" />" : NULL;
                    
                    $urls[$c]['description'] = ( empty( $urls[$c]['description'] ) ) ? __LANG_NO_DESCRIPTION__ : str_replace( "\n", '<br />', wordwrap( $urls[$c]['description'], 100 ) );
                    if ( $c == ( count( $urls ) -1 ) ) $style = ' border: none;';
                    
                    print "<td style=\"padding: 10px;width: 45px;vertical-align: top;$style\">
                           <a href=\"/browse/url?{$urls[$c]['name']}\" title=\"{$urls[$c]['title']}\">
                           <img style=\"clear:both;float:left;vertical-align: middle; width: 100px;padding: 7px;border: 1px dotted #A0A0E0;margin: 0 auto;\" src=\"{$urls[$c]['image']}\" alt=\"{$urls[$c]['mimetype']} [Icon]\" /></a>
                           </td><td style=\"padding: 15px;\"><a href=\"/browse/url?{$urls[$c]['name']}\" title=\"{$urls[$c]['title']}\">
                           <b>{$urls[$c]['title']}</b></a><br /><i>{$urls[$c]['description']}</i><br /><b>[</b>{$urls[$c]['visits_counter']} visits<b>]</b></p></td></tr>";
               }
          }
          print "</tr></table>";
     }
     
     public function getHTMLCategoryImage( $urls, $limit = 9 )
     {
          global $_SITE;

          foreach ( $urls as $id => $v )
               $urls[$id] = UrlTube_Parse::cleanUrlResults( $v );
          print "<table id=\"category_listing\"><tr>";
          for ( $c = 0; $c <= $limit; $c++ )
          {
               if ( !empty( $urls[$c] ) )
               {
                    if ( $c == 5 ) print "</tr><tr>";
                    $proto = $_SITE['UrlTube']['Parser']->getDataByURI( $urls[$c]['url'] );
                    $proto = $proto[1];
                    $proto = ( $proto == 'ftp' ) ? "<img style=\"width: 27px;vertical-align: middle;\" src=\"/public/images/categories/small.ftp.png\" alt=\"-$proto-\" />" : NULL;
                    
                    $urls[$c]['title'] = wordwrap($urls[$c]['title'], 23, '<br />', true);
                    if ( $c == ( count( $urls ) -1 ) ) $style = ' border: none;';
                    print "<td style=\"text-align:center;vertical-align: top;$style\">
                           <a href=\"/browse/url?{$urls[$c]['name']}\" title=\"{$urls[$c]['title']}\">
                           <img style=\"vertical-align: middle; width: 100px;padding: 7px;border: 1px dotted #A0A0E0;margin: 0 auto;\" src=\"{$urls[$c]['image']}\" alt=\"{$urls[$c]['mimetype']} [Icon]\" /><br />
                           <!--<img class=\"smalllink\" src=\"{$urls[$c]['image']}\" alt=\"{$urls[$c]['mimetype']} [Icon]\" /><br />-->
                           <b>{$urls[$c]['title']}</b></a><br /><i>{$urls[$c]['description']}</i><br /><b>[</b>{$urls[$c]['visits_counter']} visits<b>]</b> </td>";     
               }
          }
          print "</tr></table>";
     }

     public function getHTMLshareUrlForm( $type = 'quick' )
     {
          global $_SITE;
          
          switch ( $type )
          {
               default:
                    break;
               
               case 'quick':
                    ?><div id="addUrl">
                         <div id="page">
                         <div id="content">
                              <div class="post">
                                   <h2 class="title"><?=__LANG_HOME_ADD_TITLE__?></h2>
                                   <p class="meta"><small><?=__LANG_HOME_ADD_SUBTITLE__?></small></p>
                                   <div class="entry">
                                        <form id="addUrlForm" action="/index/share-your-url" method="post" class="niceform">
                                                  <div style="font-weight: normal; color: #333333;"><label for="add_url" class="required"><strong><?=__LANG_URI__?></strong>&nbsp;&nbsp;&nbsp;<a href="/about/url-definition" title="Help: URL Definition"><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="[?]" /></a><br />
                                                  <input onselect="javascript:" type="text" name="add_url" id="add_url" value="http://" size="128" maxlength="1024" /></label></div>
                                                  <div style="text-align: center;"><input id="addUrlSubmit" type="submit" value="<?=__LANG_SHARE__?>" /></div>
                                        </form>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div><?
                    break;
               
               case 'advanced':
                    ?><div id="addUrl">
                         <div id="page">
                         <div id="content">
                              <div class="post">
                                   <h2 class="title"><?=__LANG_ADD_TITLE__?></h2>
                                   <p class="meta"><small><?=__LANG_ADD_SUBTITLE__?></small></p>
                                   <div class="entry">
                                        <form id="addUrlForm" action="/index/share-your-url" method="post" class="niceform">
                                        <div class="left_column">
                                             <dl>
                                                  <dt style="text-align: right;"><i>* = <?=__LANG_NEEDED__?></i></dt>
                                                  <dt style="font-weight: normal; color: #333333;"><label for="add_url" class="required">*&nbsp;&nbsp;<strong><?=__LANG_URI__?></strong>&nbsp;&nbsp;&nbsp;<a href="/about/url-definition" title="Help: URL Definition"><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="[?]" /></a><br />
                                                  <input type="text" name="add_url" id="add_url" value="http://" size="57" maxlength="1023addUrl" /></label></dt> 
                         
                                                  <dt style="font-weight: normal; color: #333333;"><label style="color: #666666;" for="name" class="optional">&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=__LANG_DESIDERED_URLTUBE__?></strong>&nbsp;&nbsp;&nbsp;<a href="/ajax/under-construction?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=610" class="thickbox" title=""><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="?" /></a></label><br /> <!--( Example: 'test', will result in: http://test.urltube.net )-->
                                                  http : // <input onchange="javascript:Url.checkDomainAvailability(this);" type="text" name="name" id="name" value="" size="20" maxlength="64" style="text-align: right;" /> . <?=str_replace( '.', ' . ', $_SITE['config']['handler']->redirect->domain )?> <span id="checkDomainAvailability"></span></dt> 
                                                  
                                                  <dt><label for="add_public" class="optional"><input type="radio" name="add_public" id="add_public" class="addpublic" value="Y" />&nbsp;&nbsp;<strong><?=__LANG_PUBLIC__?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="add_public" class="addpublic" value="H" />&nbsp;&nbsp;&nbsp;<strong><?=__LANG_PRIVATE__?></strong></label>&nbsp;&nbsp;&nbsp;<a href="/ajax/under-construction?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=610" class="thickbox" title=""><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="[?]" /></a></dt>
                                                  <dt><br /><input id="addUrlSubmit" type="submit" value="<?=__LANG_SHARE__?>" /></dt>
                                             </dl>
                                        </div>
                                        <div class="right_column">
                                             <dl>
                                                  <dt><label for="add_title" class="optional"><strong><?=__LANG_TITLE__?></strong></label><br />
                                                  <input type="text" name="add_title" id="add_title" value="" size="57" maxlength="256" /></dt> 
                                                  <dt><label for="add_description" class="optional"><strong><?=__LANG_DESCRIPTION__?></strong></label><br />
                                                  <textarea name="add_description" id="add_description"  style="width: 340px;height: 100px;" cols="10" rows="3"> </textarea></dt>
                                             </dl>
                                        </div>
                                        </form>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div><?
                    break;
/*
<dt><label for="add_masquerading" class="optional">*&nbsp;&nbsp;<input type="checkbox" name="add_masquerading" id="add_masquerading" value="Y" />&nbsp;&nbsp;&nbsp;<b>URL</b> <b>M</b>asquerading?&nbsp;&nbsp;&nbsp;<a href="/ajax/under-construction?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=610" class="thickbox" title=""><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="[?]" /></a></label></dt> 

<dt><label for="add_notify_email" class="optional">*&nbsp;&nbsp;<b>E</b>-<b>M</b>ail <b>N</b>otification&nbsp;&nbsp;&nbsp;<a href="/ajax/under-construction?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=610" class="thickbox" title=""><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="[?]" /></a></label></dt> 
<dd><input type="text" name="add_notify_email" id="add_notify_email" value="" size="40" maxlenght="64" /></dd> 

<dt><label for="add_irc_notify" class="optional">*&nbsp;&nbsp;<input type="checkbox" name="add_irc_notify" id="add_irc_notify" value="Y" />&nbsp;&nbsp;&nbsp;<b>IRC N</b>otification&nbsp;&nbsp;&nbsp;<a href="/ajax/under-construction?KeepThis=true&amp;TB_iframe=true&amp;height=400&amp;width=610" class="thickbox" title=""><img style="width:10px;vertical-align: top;" src="/public/images/help.png" alt="[?]" /></a></label></dt> 

<dt><label for="add_irc_notify_hostname" class="optional">*&nbsp;&nbsp;<b>IRC N</b>otification - <b>S</b>erver <b>H</b>ostname</label></dt> 
<dd><input type="text" name="add_irc_notify_hostname" id="add_irc_notify_hostname" value="" maxlenght="64" /></dd> 

<dt><label for="add_irc_notify_port" class="optional">*&nbsp;&nbsp;<b>IRC N</b>otification - <b>S</b>erver <b>P</b>ort</label></dt> 
<dd><input type="text" name="add_irc_notify_port" id="add_irc_notify_port" value="6667" size="5" maxlength="5" /></dd> 

<dt><label for="add_irc_notify_channel" class="optional">*&nbsp;&nbsp;<b>IRC N</b>otification - <b>S</b>erver <b>C</b>hannel</label></dt> 
<dd><input type="text" name="add_irc_notify_channel" id="add_irc_notify_channel" value="#" maxlenght="32" /></dd> 
*/

          }
//      print "<script type=\"text/javascript\">
//      //<![CDATA[          
//     window.addEvent('domready', function(){
//
//          $('addUrlSubmit').addEvent('click', function(e) {
//               new Event(e).stop();
//               UrlTube.submitForm('addUrlForm','addUrl', '/ajax/share-your-url');
//          });
//     });
//      //]]>
//               </script>";
     }
     
     //
     // OBIETTIVI: CODING CLASSE LANGUAGE RECOGNIZER
     //            CMS MVC BASED
     //            ZEND INTEROPERABILITY
     //            FLASH PLAYERS
     //            CONTENT MANIPULATING AND PUBLISHING
     //            CRAWLER BOT FUNCTIONS
     //            EXPERIMENT SOME SYSTEM TO KEEP OLD CLIENT WORKING ON SITE AS PC AND MOBILE DEVICES WORKING!
     //            NELLA VISUALIZZAZIONE DELL'URL POSSIBILITA' ULTERIORI NOTIFICHE ( CON PROTEZIONE )
     //            GET FUN
     // Aggiunta controlli vari tipo:
     // - uno che clicca piu' volte di fila su submit.
     // Rilascio versione 1 senza AJAX
     // Sviluppo AJAX e malizie varie 2.0
     // Sviluppo navigazione categorie 2.1
     // Sviluppo Crawler 2.2
     // Sviluppo Tools vari <=2.9
     // Sviluppo ambiente mobile 3.0
     
     /*
     public function checkUrl( $url )
     {
          // @todo 1 ONLINE - Controllo status dell'url ( prima dalla data sul db per last_status_update a meno di 3600*7 altrimenti recheck and update last_status_update ) in caso di 404 o 500 Restituire una pagina di errore interna nostra ( courtesy page )
          if ( $url['status'] != '200' ) $code .= '1,';
          // @todo 2 ADSERVER - Controllare se � la un multiplo di 5 per visualizzare la pubblicita' con timer per redirect
          if ( ( $url['counterVisit'] / 10 ) != '200' ) $code .= '2,';
          // @todo Log del visionamento
          // @todo 3 INTRO - Check se da visualizzare l'introduzione
          if ( ( $url['counterVisit'] / 10 ) != '200' ) $code .= '3,';
          // @todo 4 IFRAME - Check se � da visualizzare tramite iFrame o redirect
          if ( ( $url['counterVisit'] / 10 ) != '200' ) $code .= '4,';
          // @todo 5 MOBILE - Sistema iFrame/redirect compatibile per old mobile devices
          if ( ( $url['counterVisit'] / 10 ) != '200' ) $code .= '5,';
          // @todo 6 CENSURA - Controllo l'activeness per evitare roba censurata
          if ( ( $url['counterVisit'] / 10 ) != '200' ) $code .= '6,';
     }
     */
}

$_SITE['UrlTube']['HTML'] = new UrlTube_HTML();
