<?php

class UrlTube_Feeds
{
    public $mimetype  = NULL;
    public $mime      = NULL;
    public $type      = NULL;
    
    public $service   = NULL;
    public $protocol  = NULL;
    public $feed      = NULL;
    public $feed_type = 'rss';
    public $feed_handler = NULL;
    
    public $title = NULL;
    public $description = NULL;
    
    
    public function UrlTube_Feeds( $options = array( ) )
    {
    	foreach ( $options as $option => $value )
    		$this->$option = $value;
    }
    
    public function getUrlTubes( )
    {
    	return UrlTube::getUrls( array( 'name', 'visits_counter', 'creation_date' ), array( 'url', 'mimetype' ), NULL, array( 'links.creation_date DESC' ), 'Y', $this->mimetype, $this->service, $this->protocol, array( 'limit' => 10 ) );
    }
    
    public function getFeed( $type = NULL )
    {
    	if ( ! is_null( $type ) ) $this->feed_type = $type;
    	
    	$UrlTubes = $this->getFeedArrayByUrlTubes( $this->getUrlTubes( ), $this->service );
    	
    	switch( $this->feed_type )
    	{
    		default:
    		case 'rss':
				$this->feed_handler = Zend_Feed::importArray( $UrlTubes, 'rss' );
    			break;
    			
    		case 'atom':
				$this->feed_handler = Zend_Feed::importArray( $UrlTubes, 'atom' );
    			break;
    	}
    	
    	$this->feed = $this->feed_handler->saveXML( );
    	
    	return $this->feed;
    }
    
    public function printFeed(  )
    {
    	if ( is_null( $this->feed_handler ) ) $this->getFeed( $this->feed_type );
    	
    	$this->feed_handler->send( );
    	
    	return $this->feed_handler;
    }
    
    public function getFeedInstance(  )
    {
    	if ( is_null( $this->feed_handler ) ) $this->getFeed( $this->feed_type );
    	
    	return $this->feed_handler;
    }
    
    public function getFeedArrayByUrlTubes( $urls = NULL, $service = NULL )
    {
    	global $_SITE;
    	
    	if ( is_null( $urls ) ) return FALSE;
    	
    	$feedArray = $this->getFeedArray( );

    	$feedArray['lastUpdate'] = $urls[0]['creation_date'];
        $feedArray['published']  = '454475222';
		$feedArray['itunes'] = array(
                     'author'       => trim( $_SITE['config']['handler']->site->admin ), // optional, default to the main author value
                     'image'        => 'http://' . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . '/public/themes/default/images/logo.png', // optional, default to the main image value
                     'subtitle'     => trim( $this->description ), // optional, default to the main description value
                     'summary'      => trim( $this->description ), // optional, default to the main description value
                     'explicit'     => 'clean', // optional
                     'category'     => array(
                                             array('main' => 'Short Internet Links', // required
                                                   'sub'  => 'Audio category' // optional
                                                   )
                                             // up to 3 rows
                                             ) // 'Category column and in iTunes Music Store Browse' // required*/
                     /*'owner'        => array(
                                             'name' => 'name of the owner', // optional, default to main author value
                                             'email' => 'email of the owner' // optional, default to main email value
                                             ), */// Owner of the podcast // optional
                     //'block'        => 'Prevent an episode from appearing (yes|no)', // optional
                     //'keywords'     => 'a comma separated list of 12 keywords maximum', // optional
                     //'new-feed-url' => 'used to inform iTunes of new feed URL location' // optional
                     );
                     
        $feedArray['entries'] = array( );

        if ( ! empty( $urls[0] ) )
        foreach( $urls as $id => $url )
        {
    	    if ( ! empty( $url['name'] ) )
    	    {
				$url['title'] = ( is_null( $url['title'] ) ) ? __LANG_NO_TITLE__ : str_replace( "\n", '<br />', wordwrap( $url['title'], 25 ) );
				$url['description'] = ( is_null( $url['description'] ) ) ? __LANG_NO_DESCRIPTION__ : str_replace( "\n", '<br />', wordwrap( $url['description'], 35 ) );
//				$url['creation_date'] = date( DATE_RFC822, $url['creation_date'] );
//				
//				print "CCC".$url['creation_date']."CCC";
		        $feedArray['entries'][$id]  = array('title'        => trim( strip_tags( $url['title'] ) ),
				                                   'link'         => 'http://www.'.$_SITE['config']['handler']->redirect->domain.'/browse/url?'.$url['name'],
				                                   'description'  => trim( strip_tags( $url['description'] ) ) . ' ', // only text, no html, required
				                                   'lastUpdate'   => $url['creation_date'], // optional
				                                   'pubDate'      => $url['creation_date'], // optional
				                                   'source'       => array(
				                                                           'title' => trim( $url['title'] ), // required,
				                                                           'url' => trim( $url['url'] ) // required
				                                                           ), // original source of the feed entry // optional
				                                   'guid'         => trim( 'http://www.'.$_SITE['config']['handler']->redirect->domain.'/browse/url?'.$url['name'] ),
				                                   'content'      => trim( strip_tags( $url['description'] ) ) . '&nbsp;',
				                                   'enclosure'    => array( array( 'lenght' => $url['size'], 'type' => $url['mimetype'], 'url' => 'http://www.'.$_SITE['config']['handler']->redirect->domain.'/browse/url?'.$url['name'] ) )/*,
				                                   //'comments'     => 'comments page of the feed entry', // optional
				                                   //'commentRss'   => 'the feed url of the associated comments', // optional
				                                   */ );
    	    }
        }
        
    	return $feedArray; 
    }
    
    public function getFeedArray( )
    {
    	global $_SITE;

		$feedArray = array(
		      'title'       => trim( $this->title ),
		      'link'        => 'http://' . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'],
		      'charset'     => trim( $_SITE['config']['handler']->site->charset ),
		      'description' => trim( $this->description ),
		      'author'      => trim( $_SITE['config']['handler']->site->admin ),
		      'email'       => trim( $_SITE['config']['handler']->site->admin ),
		      'webmaster'   => trim( $_SITE['config']['handler']->site->admin ),
		      'image'       => 'http://' . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . '/public/themes/default/images/logo.png',
		      'generator'   => trim( __GENERATOR__ ),
		      'language'    => trim( $_SITE['config']['env']['language']['short'] ), // optional
		      'ttl'         => '3600',
		      'copyright'   => '@'.date('Y').' UrlTube, in exception of the original URL that is belong to its author'
		      
		      //'rating'      => 'The PICS rating for the channel.',
		      /*'textInput'   => array(
		                             'title'       => 'the label of the Submit button in the text input area', // required,
		                             'description' => 'explains the text input area', // required
		                             'name'        => 'the name of the text object in the text input area', // required
		                             'link'        => 'the URL of the CGI script that processes text input requests', // required
		                             ),*/ // a text input box that can be displayed with the feed // optional, ignored if atom is used
		       );
		 return $feedArray;
    }
	
	public function getSitemap( $categoryAlso = TRUE )
	{
		global $_SITE;
		//_dump($_SITE);
		$links = $_SITE['database']['handler']->select( )
		                                     ->from( $_SITE['config']['handler']->sitemap->database->table )
		                                     ->order( array( 'lastmod DESC', 'priority DESC' ) );
		$handle = $links->query( );
		$links = $handle->fetchAll( );

		foreach( $links as $link )
			if ( ! @in_array( $linkz[$link['controller']], $link['action'] ) )
				$linkz[$link['controller']][$link['action']] = $link;
				
		foreach( $links as $link )
			if ( ! @in_array( $link['controller'], $links_controllers ) )
				$links_controllers[] = $link['controller'];

		foreach( $links as $link )
			if ( ! @in_array( $link['action'], $actions[$link['controller']] ) )
			$actions[$link['controller']][] = $link['action'];

//			_dump($links_controllers);
//			_dump($actions);
			
    	$host = ( $_SERVER['SERVER_PORT'] == '80' ) 
    	      ? $_SERVER['HTTP_HOST'] : "{$_SERVER['HTTP_HOST']}:{$_SERVER['SERVER_PORT']}";

		foreach ( glob( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . '*' ) as $controller )
		{
			$controller = str_replace( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR, NULL, $controller );
			//_dump('C'.$controller);
			// *** Check if the controller exists on database
			if ( @in_array( $controller, $links_controllers ) !== FALSE )
			{
				foreach( glob( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . 
				               $controller . DIRECTORY_SEPARATOR . '*' ) as $action )
				{
					$patterns[0] = '/'.str_replace( '/', '\\/', $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . 
					                                            $controller . DIRECTORY_SEPARATOR ).'/';
					$patterns[1] = '/.phtml/';
					$replacements[0] = NULL;
					$replacements[1] = NULL;
					$action = preg_replace( $patterns, $replacements, $action );
//					_dump($action);
					// *** Check if the action exists on database				
					if ( @in_array( $action, $actions[$controller] ) !== FALSE ) 
					{
					    	unset( $url );
					    	if ( $action != 'index' ) $url = "$controller/$action";
					    	elseif ( $controller != 'index' && $action == 'index' ) $url = $controller;
//					    	_dump($linkz[$controller][$action]);
							$linkz[$controller][$action]['lastmod'] = array_pop( array_reverse( explode( ' ', $linkz[$controller][$action]['lastmod'] ) ) );
							
					    	if ( $linkz[$controller][$action]['published'] == 'Y' && ! @array_search( "http://$host/$url", $urls[$controller] ) )
					    	$urls[$controller][] = array( 'loc' => "http://$host/$url", 
					    	                              'lastmod' => date( 'Y-m-' ) . ( date( 'd' ) - 2 ),//$linkz[$controller][$action]['lastmod'], 
					    	                              'changefreq' => $linkz[$controller][$action]['changefreq'], 
					    	                              'priority' => $linkz[$controller][$action]['priority'] );
					    	                                 
					    // *** If not present we create a new database entry
					} else $_SITE['database']['handler']->insert( $_SITE['config']['handler']->sitemap->database->table, array( 'controller' => $controller, 'action' => $action ) );
				}
				// *** If not present we create a new database entry
			} else $_SITE['database']['handler']->insert( $_SITE['config']['handler']->sitemap->database->table, array( 'controller' => $controller, 'action' => 'index' ) );
		}
		
		if ( $categoryAlso )
		foreach ( explode( ',', $_SITE['config']['handler']->categories ) as $category )
		{
			$category = trim( $category );
			$urls['browse'][] = array( 'loc' => "http://$host/browse/categories?$category", 
		                              'lastmod' => date('Y-m-'). ( date('d') - 1 ), 
		                              'changefreq' => 'Daily', 
		                              'priority' => '0.7' );
		}
		
		return $urls;
	}
} 