/*
MySQL Data Transfer
Source Host: localhost
Source Database: mydns
Target Host: localhost
Target Database: mydns
Date: 11/10/2008 2.14.34
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for sitemap
-- ----------------------------
CREATE TABLE `sitemap` (
  `id` int(11) NOT NULL auto_increment,
  `controller` varchar(32) NOT NULL,
  `action` varchar(32) default 'index',
  `module` varchar(32) default 'default',
  `lastmod` varchar(26) default '2008-10-01T19:43:25+00:00',
  `changefreq` varchar(10) default 'weekly',
  `priority` varchar(3) default '0.3',
  PRIMARY KEY  (`id`),
  KEY `Idx1` (`id`,`controller`,`action`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
